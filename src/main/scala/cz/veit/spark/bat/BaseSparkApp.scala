package cz.veit.spark.bat

import org.apache.log4j.{Level, Logger}
import org.rogach.scallop.ScallopConf

class VeitCommandLine(appName:String, arguments: Seq[String]) extends ScallopConf(arguments)
{
  version(appName + " (c) 2018 Veit Electronics")
  footer("\nFor more tricks consult the documentation!")
}


abstract class  BaseSparkApp() extends App{
//  Logger.getLogger("com").setLevel(Level.WARN)
//  Logger.getLogger("org").setLevel(Level.WARN)
//  Logger.getLogger("akka").setLevel(Level.WARN)
}
