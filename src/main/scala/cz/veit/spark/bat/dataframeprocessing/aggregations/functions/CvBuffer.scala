package cz.veit.spark.bat.dataframeprocessing.aggregations.functions

case class CvBuffer(xSum: Double, x2Sum: Double, count: Long)
