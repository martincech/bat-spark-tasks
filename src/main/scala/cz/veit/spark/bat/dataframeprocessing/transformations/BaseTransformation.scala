package cz.veit.spark.bat.dataframeprocessing.transformations

import cz.veit.spark.bat.dataframeprocessing.BaseProcessing
import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.StreamingQuery


abstract class BaseTransformation(dataSource: BaseDataSource,
                                  dataSink: BaseDataSinkFactory,
                                  sparkSession: SparkSession,
                                  extendedStats: Boolean = true)
  extends BaseProcessing(dataSource, dataSink, sparkSession) {

  def transform(descriptor: String): StreamingQuery
}


