package cz.veit.spark.bat.dataframeprocessing.datasinks.influxdb
import com.github.fsanaulla.chronicler.core.enums.{Consistencies, Precisions}
import com.github.fsanaulla.chronicler.core.model.InfluxFormatter
import com.github.fsanaulla.chronicler.macros.Influx
import com.github.fsanaulla.chronicler.macros.annotations.{field, tag, timestamp}
import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseAggregationSink
import cz.veit.spark.bat.dataobjects.AggregationResult
import org.apache.log4j.LogManager
import org.joda.time.DateTime

import scala.concurrent.duration._
import scala.util.{Failure, Success}

class InfluxAggregationSink(address: String)
  extends BaseAggregationSink
    with BaseInfluxDataSink {
  override def conString: String = address
  final case class Point(
     @tag sex: String,
     @field average: Double,
     @field count: Int,
     @field cv: Double,
     @field gain: Double,
     @field min: Double,
     @field max: Double,
     @field uniformity: Double,
     @field sigma: Double,
     @timestamp timestamp: Long)
  implicit val fmt: InfluxFormatter[Point] = Influx.formatter[Point]

  def createPoint(record: AggregationResult, interval: Long): Point = {
    var sex = ""
    try {
      if (record.sex != null && !record.sex.isEmpty) {
        sex =  record.sex
      }
    } catch {
      case e: IllegalArgumentException =>
    }
    val timestamp = new DateTime(record.intervalStart).getMillis
     Point(
       sex,
       record.average,
       record.count.toInt,
       record.cv,
       record.gain,
       record.min,
       record.max,
       record.uniformity,
       record.sigma,
       timestamp)
  }

  def process(record: AggregationResult): Unit = {
      val normalized = normalizeRecord(record)
      val interval = (normalized.intervalEnd.getTime - normalized.intervalStart.getTime).milliseconds.toMinutes
      val retentionPolicy = interval + "_m"

      val measName = (record.id :: InfluxConstants.grouppedDataToMeasurement(record.topic) :: interval + "m" :: Nil).mkString("_")
      val dbName = record.database
      val meas = influxDb.measurement[Point](dbName, measName)
      val value = createPoint(normalized, interval)
      meas.write(value,
        Option(Consistencies.ONE),
        Option(Precisions.MILLISECONDS), Option(retentionPolicy))
        match {
          case Success(v)  => LogManager.getRootLogger.error("Successfully pushed data to influx: " + v)
          case Failure(ex) => throw ex
        }
  }
}
