package cz.veit.spark.bat.dataframeprocessing.datasinks.kafka

import java.util.concurrent.Future

import cz.veit.spark.bat.KafkaProps
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord, RecordMetadata}
import org.apache.spark.sql.ForeachWriter

trait KafkaSink[I, O]
  extends ForeachWriter[I] {
  private var producer: KafkaProducer[String, O] = _

  def broker: String

  protected def send(topic: String, key: String, data: O): Future[RecordMetadata] = {
    producer.send(
      new ProducerRecord(
        topic,
        key,
        data))
  }

  override def open(partitionId: Long, version: Long): Boolean = {
    producer = new KafkaProducer[String, O](KafkaProps(broker))
    true
  }

  override def close(errorOrNull: Throwable): Unit = {
    producer.close()
  }

}
