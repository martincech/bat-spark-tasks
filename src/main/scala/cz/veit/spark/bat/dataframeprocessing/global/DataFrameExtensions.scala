package cz.veit.spark.bat.dataframeprocessing.global

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.sql.types.StructType

import scala.reflect.runtime.universe.TypeTag

class DataFrameExtensions(dataFrame: DataFrame) {

  def selectDataAndTopic[T: TypeTag](): DataFrame = {
    val structType = ScalaReflection.schemaFor[T].dataType.asInstanceOf[StructType]
    selectDataAndTopic(structType)
  }

  def selectDataAndTopic(dataStruct: StructType): DataFrame = {
    filterValidTopic().select(
      from_json(col("json"),
        dataStruct,
        Map("allowUnquotedFieldNames" -> true.toString)
      ) as "data", col("topic"), col("key"), col("recordTime"))
      .select(col("data.*"), col("topic") as "__topic__", col("key"), col("recordTime"))
      .withColumnRenamed("topic", "datatopic")
      .withColumnRenamed("__topic__", "topic")
  }

  private def filterValidTopic(): DataFrame = {
    dataFrame.select(col("value") cast "string" as "json", col("topic"), col("key") cast "string" as "key", col("timestamp") cast "Timestamp" as "recordTime")
      .filter(col("topic").isNotNull && col("topic") =!= "")
  }

  def toLower(df: DataFrame): DataFrame = {
    df.toDF(df.columns map (_.toLowerCase): _*)
  }
}
