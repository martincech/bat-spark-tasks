package cz.veit.spark.bat.dataframeprocessing.datasources

import org.apache.spark.sql.DataFrame

trait BaseDataSource {
  def df: DataFrame
}


