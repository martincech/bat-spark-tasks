package cz.veit.spark.bat.dataframeprocessing

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import org.apache.spark.sql.{DataFrame, SparkSession}

abstract class BaseProcessing(dataSource: BaseDataSource,
                              dataSink: BaseDataSinkFactory,
                              sparkSession: SparkSession) extends Serializable {
  protected val df: DataFrame = dataSource.df

}
