package cz.veit.spark.bat.dataframeprocessing.datasinks.kafka

import java.sql.Timestamp
import cz.veit.spark.bat.dataobjects.topicnames.RawDataTopic
import cz.veit.spark.bat.dataobjects.{AggregationResult, DailyWeightAggregation}

class KafkaGroupedDailyWeightSink(brokerAddress: String,
                                  redisHost: String,
                                  redisPort: Integer,
                                  topicTransform: String => String = s => s,
                                  idTransform: String => String = s => s)
  extends KafkaRedisSink[DailyWeightAggregation, AggregationResult] {

  override def host: String = redisHost

  override def port: Integer = redisPort

  override def broker: String = brokerAddress

  override def mapToOutput(group: String, database: String, record: DailyWeightAggregation): AggregationResult = {
    AggregationResult(
      group,
      database,
      record.TimeStamp,
      record.TimeStamp,
      record.Sex,
      topicForRecord(record),

      record.Count,
      record.Average,
      record.Average, record.Average, //aka MIN and MAX fake
      record.Sigma,
      record.Cv,
      record.Gain,
      record.Uniformity
    )
  }

  override def timestampForRecord(record: DailyWeightAggregation): Timestamp = {
    record.TimeStamp
  }

  override def topicForRecord(record: DailyWeightAggregation): String = {
    topicTransform(RawDataTopic.Weight.toString)
  }

  override def idForRecord(record: DailyWeightAggregation): String = {
    record.Uid
  }

  override def transformedIdForRecord(record: DailyWeightAggregation): String = {
    idTransform(record.Uid)
  }
}
