package cz.veit.spark.bat.dataframeprocessing.datasinks.log4j

import cz.veit.spark.bat.serialization.JsonSerializer
import org.apache.log4j.Logger
import org.apache.spark.sql.ForeachWriter

trait Log4jDataSink[T] extends ForeachWriter[T] {
  def logger : Logger

  def process(record: T): Unit = {
    logger.info(JsonSerializer.toJson(record))
  }

  def open(partitionId: Long, version: Long): Boolean = true

  def close(errorOrNull: Throwable): Unit = {}
}
