package cz.veit.spark.bat.dataframeprocessing.aggregations.functions

import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.{Encoder, Encoders, Row}

class GaussUniformity(averageName: String, sigmaName: String, countName: String, uniformityRange: Double = 10)
  extends Aggregator[Row, (Double, Double, Double), Double]
    with Serializable {

  private val MIN_SIGMA = 1e-03f
  private val INV_SQRT_2_PI = 0.3989422804f // 1/(SQRT(2*PI))
  private val POLY_P = 0.231641900f
  private val POLY_B = 0.319381530f :: -0.356563782f :: 1.781477937f :: -1.821255978f :: 1.330274429f :: Nil

  private def calcUniformity(average: Double, sigma: Double): Double = {
    if (sigma < MIN_SIGMA) return 100 // "deleni nulou"
    val x = uniformityRange / 100.0f * average / sigma
    if (x > 3) return 100 // >3*sigma, vsechny hodnoty v toleranci
    val gi = gaussIntegral(x, gauss(x))
    (1-(gi*2))*100.0f
  }

  private def gauss(x: Double): Double = INV_SQRT_2_PI * Math.pow(Math.E, -x * x / 2)

  private def gaussIntegral(x: Double, gaussx: Double): Double = {
    // gaussova krivka integrovana od <x> do +oo, <gaussx> je hodnota gaussovy
    // krivky pro <x>
    val t = 1 / (1 + POLY_P * x)
    gaussx * POLY_B.zipWithIndex.map(poly => poly._1 * Math.pow(t, poly._2 + 1)).sum
  }

  override def zero: (Double, Double, Double) = (0.0, 0.0, 0.0)

  override def reduce(buf: (Double, Double, Double), input: Row): (Double, Double, Double) = {
    val avg = input.getAs[Double](averageName)
    val sigma = input.getAs[Double](sigmaName)
    val count = input.getAs[Double](countName)
    (avg, sigma, count)
  }

  private def weightedAvg(val1: Double, val2: Double, count1: Double, count2: Double) = {
    if ((count1 + count2) == 0) 0.0 else (val1 * count1 + val2 * count2) / (count1 + count2)
  }

  override def merge(b1: (Double, Double, Double), b2: (Double, Double, Double)): (Double, Double, Double) = {
    val avg = weightedAvg(b1._1, b2._1, b1._3, b2._3)
    val sigma = weightedAvg(b1._2, b2._2, b1._3, b2._3)
    (avg, sigma, b2._3 + b1._3)
  }

  override def finish(b: (Double, Double, Double)): Double = {
    calcUniformity(b._1, b._2)
  }

  override def bufferEncoder: Encoder[(Double, Double, Double)] = Encoders.tuple(Encoders.scalaDouble,Encoders.scalaDouble,Encoders.scalaDouble)

  override def outputEncoder: Encoder[Double] = Encoders.scalaDouble
}
