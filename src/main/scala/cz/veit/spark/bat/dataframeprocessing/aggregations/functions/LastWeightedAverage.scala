package cz.veit.spark.bat.dataframeprocessing.aggregations.functions

import java.sql.Timestamp

import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.{Encoder, Encoders, Row}

class LastWeightedAverage(valueName: String, countZeroValues: Boolean = true)
  extends Aggregator[Row, Map[String, (Timestamp, Double, Double, Timestamp)], Double]
    with Serializable {

  private def getNewest(oldVal: (Timestamp, Double, Double, Timestamp), newVal: (Timestamp, Double, Double, Timestamp)): (Timestamp, Double, Double, Timestamp) = {
    if (oldVal._1.after(newVal._1) || (oldVal._1 == newVal._1 && oldVal._4.after(newVal._4))) oldVal
    else newVal
  }

  override def zero: Map[String, (Timestamp, Double, Double, Timestamp)] = Map[String, (Timestamp, Double, Double, Timestamp)]()

  override def reduce(map: Map[String, (Timestamp, Double, Double, Timestamp)], input: Row): Map[String, (Timestamp, Double, Double, Timestamp)] = {
    val key = input.getAs[String]("key")
    val timestamp = input.getAs[Timestamp]("timestamp")
    val value = input.getAs[Double](valueName)
    val count = input.getAs[Double]("count")
    val recordTime =  input.getAs[Timestamp]("recordTime")

    val newVal = (timestamp, value, count, recordTime)
    val oldVal = map.getOrElse(key, newVal)

    map + (key -> getNewest(oldVal, newVal))
  }

  override def merge(map1: Map[String, (Timestamp, Double, Double, Timestamp)], map2: Map[String, (Timestamp, Double, Double, Timestamp)]): Map[String, (Timestamp, Double, Double, Timestamp)] = {
    val result = map1 ++ map2.map(
      item => {
        val newV = item._2
        val oldV = map1.getOrElse(item._1, newV)
        item._1 -> getNewest(oldV, newV)
      })
    result
  }

  override def finish(reduction: Map[String, (Timestamp, Double, Double, Timestamp)]): Double = {
    val map =
      if (!countZeroValues) {
        reduction.filter(v => v._2._2 != 0)
      } else {
        reduction
      }
    val count = map.foldLeft(0.0)((a, b) => a + b._2._3)
    if (count == 0) {
      Double.NaN
    } else {
      map.foldLeft(0.0)((a, b) => a + (b._2._2 * b._2._3)) / count
    }
  }

  override def bufferEncoder: Encoder[Map[String, (Timestamp, Double, Double, Timestamp)]] =
    Encoders.kryo[Map[String, (Timestamp, Double, Double, Timestamp)]]

  override def outputEncoder: Encoder[Double] = Encoders.scalaDouble
}
