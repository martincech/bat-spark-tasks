package cz.veit.spark.bat.dataframeprocessing.datasinks

import cz.veit.spark.bat.dataobjects.AggregationResult
import org.apache.spark.sql.ForeachWriter

abstract class BaseAggregationSink extends ForeachWriter[AggregationResult] {
  def normalizeRecord(record: AggregationResult): AggregationResult = {
    AggregationResult(
      record.id,
      record.database,
      record.intervalStart,
      record.intervalEnd,
      record.sex,
      record.topic,

      if (record.count.isNaN || record.count.isInfinity) 0.0 else record.count,
      if (record.average.isNaN || record.average.isInfinity) 0.0 else record.average,
      if (record.min.isNaN || record.min.isInfinity) 0.0 else record.min,
      if (record.max.isNaN || record.max.isInfinity) 0.0 else record.max,
      if (record.sigma.isNaN || record.sigma.isInfinity) 0.0 else record.sigma,
      if (record.cv.isNaN || record.cv.isInfinity) 0.0 else record.cv,
      if (record.gain.isNaN || record.gain.isInfinity) 0.0 else record.gain,
      if (record.uniformity.isNaN || record.uniformity.isInfinity) 100 else record.uniformity
    )
  }
}


