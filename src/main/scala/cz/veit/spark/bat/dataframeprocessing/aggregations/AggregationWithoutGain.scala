package cz.veit.spark.bat.dataframeprocessing.aggregations

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import cz.veit.spark.bat.dataframeprocessing.global.TimeConstants
import cz.veit.spark.bat.dataobjects.{AggregationResult, TransformedRawDataGrouped}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.StreamingQuery

import scala.concurrent.duration._
class AggregationWithoutGain(baseDataFrame: BaseDataSource,
                             dataSink: BaseDataSinkFactory,
                             sparkSession: SparkSession,
                             extendedStats: Boolean = true)
  extends BaseAggregation(baseDataFrame, dataSink, sparkSession, extendedStats) {
  def aggregate(duration: FiniteDuration, descriptor: String): StreamingQuery = {
    import Extensions._
    import sparkSession.implicits._

    df.selectDataAndTopic[TransformedRawDataGrouped]()
      .withColumnRenamed("groupId", "id")
      .watermark()
      .deduplicate("timestamp", "topic", "database", "id", "sex")
      .groupBy(
        window($"timestamp", duration.toString()),
        $"topic", $"database", $"id", $"sex")
      .aggregate()
      .withColumn("gain", lit(0))
      .withColumn("intervalStart", $"window.start")
      .withColumn("intervalEnd", $"window.end")
      .as[AggregationResult]
      .start(TimeConstants.AggregationWithoutGainTriggerDuration, descriptor, duration)
  }
}
