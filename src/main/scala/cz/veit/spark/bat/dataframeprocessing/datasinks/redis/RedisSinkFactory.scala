package cz.veit.spark.bat.dataframeprocessing.datasinks.redis

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataobjects.GroupEvent
import org.apache.spark.sql.ForeachWriter
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.streaming.DataStreamWriter

import scala.reflect.runtime.universe.TypeTag

object RedisSinkFactory {


  def apply(redisHost: String, redisPort: Integer): RedisSinkFactory = {
    new RedisSinkFactory(redisHost, redisPort)
  }
}

final class RedisSinkFactory(redisHost: String, redisPort: Integer)
  extends BaseDataSinkFactory {

  def setupWriter[T: TypeTag](sw: DataStreamWriter[T]): DataStreamWriter[T] = {
    if (ScalaReflection.dataTypeFor[T] == ScalaReflection.dataTypeFor[GroupEvent]) {
      return sw.foreach(new GroupRedisSink(redisHost, redisPort).asInstanceOf[ForeachWriter[T]])
    }
    throw new NotImplementedError("Redis sink not implemented for type:" + ScalaReflection.dataTypeFor[T].typeName)
  }
}