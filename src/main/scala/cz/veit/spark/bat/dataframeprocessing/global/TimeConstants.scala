package cz.veit.spark.bat.dataframeprocessing.global

import scala.concurrent.duration.{FiniteDuration, _}

object TimeConstants extends Serializable {
  /*---------------------------------------------------------------------------
   * Batch processing
   * ---------------------------------------------------------------------------*/
  val GroupTopicTriggerDuration: FiniteDuration = 15.seconds
  // Batch processing  - how often run the trigger for data in GroupTopic
  val DailyWeightTransformationTriggerDuration: FiniteDuration = 15.minutes
  // Batch processing  - how often run the trigger for data in DailyWeight topic
  val RawDataTransformationTriggerDuration: FiniteDuration = 15.minutes
  // Batch processing  - how often run the trigger for data in Raw data topic
  val AggregationWithGainTriggerDuration: FiniteDuration = 5.minutes
  // Batch processing  - how often run the trigger for data in Aggregation with gain topic
  val AggregationWithoutGainTriggerDuration: FiniteDuration = 5.minutes
  // Batch processing  - how often run the trigger for data in Aggregation without gain topic
  val AggregationAggregatedTriggerDuration: FiniteDuration = 5.minutes
  // Batch processing  - how often run the trigger for data in Aggregation of aggregated topic

  /*---------------------------------------------------------------------------
   * Aggregation windows
   * ---------------------------------------------------------------------------*/
  val WatermarkDurationLong: FiniteDuration = 7.days
  // Aggregation window - watermark duration for data with long period (important data)
  val WatermarkDurationShort: FiniteDuration = 8.hours
  // Aggregation window - watermark duration for data with short period (unimportant data)
  val AcceptedFutureStampLong: FiniteDuration = 1.day
  // Aggregation window - maximum time in the future to be accepted for data with long period (important data)
  val AcceptedFutureStampShort: FiniteDuration = 1.hour
  // Aggregation window - maximum time in the future to be accepted for data with short period (unimportant data)
  val AggregationWindowShort: FiniteDuration = 30.minutes
  // Aggregation window - short period
  val AggregationWindowLong: FiniteDuration = 1.day
  // Aggregation window - long period

  /*---------------------------------------------------------------------------
   * Spark queries
   * ---------------------------------------------------------------------------*/
  val QueryRestartDelay: FiniteDuration = 5.minutes
  // Spark queries - how long to wait in between restart of failed query
}
