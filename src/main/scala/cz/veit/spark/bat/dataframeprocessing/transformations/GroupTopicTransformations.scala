package cz.veit.spark.bat.dataframeprocessing.transformations

import cz.veit.spark.bat.dataframeprocessing.datasinks.{BaseDataSinkFactory, DataSinkWriter}
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import cz.veit.spark.bat.dataframeprocessing.global.{DataFrameExtensions, TimeConstants}
import cz.veit.spark.bat.dataobjects.GroupEvent
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
class GroupTopicTransformations(dataSource: BaseDataSource,
                                dataSink: BaseDataSinkFactory,
                                sparkSession: SparkSession)
  extends BaseTransformation(dataSource, dataSink, sparkSession) {

  private class GroupTopicExtensions(df: DataFrame)
    extends DataFrameExtensions(df) {

    def filterMalformedData(): DataFrame = {
      df.filter(
        col("uid").isNotNull && col("uid") =!= ""
          && col("database").isNotNull && col("database") =!= ""
      )
    }

    def selectDataAndTopic(): DataFrame = {
      toLower(selectDataAndTopic[GroupEvent]())
    }
  }

  private object GroupTopicExtensions {
    implicit def dataSet(ds: DataFrame): GroupTopicExtensions = new GroupTopicExtensions(ds)
    implicit def writer(ds: Dataset[GroupEvent]): DataSinkWriter[GroupEvent] = new DataSinkWriter[GroupEvent](ds, dataSink)
  }


  def transform(descriptor: String): StreamingQuery = {
    import GroupTopicExtensions._
    import sparkSession.implicits._
    df.selectDataAndTopic()
      .filterMalformedData()
      .as[GroupEvent]
      .start(TimeConstants.GroupTopicTriggerDuration, descriptor)
  }

}
