package cz.veit.spark.bat.dataframeprocessing.global

import scala.concurrent.duration.FiniteDuration

object Descriptors {
  def newCheckpointName(id: String): String = {
    HDFSStore.hdfsPath + "checkpoint/" + id
  }

  def unifyDescriptor(base: String, interval: FiniteDuration = null, additionalDescr: String = ""): String = {
    ((if (interval == null) "" else interval.toString()) ::
      base ::
      (if (additionalDescr == null) "" else additionalDescr) ::
      Nil).filterNot(s => s.isEmpty)
      .mkString("_")
      .filterNot(x => x.isWhitespace)
  }
}
