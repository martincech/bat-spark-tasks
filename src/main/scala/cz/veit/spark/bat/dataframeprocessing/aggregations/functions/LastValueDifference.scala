package cz.veit.spark.bat.dataframeprocessing.aggregations.functions

import java.sql.Timestamp

class LastValueDifference(valueName: String)
  extends LastValue(valueName) {

  override def finish(map: Map[Timestamp, (Timestamp, Double)]): Double = {
    val sortedList = map.toSeq.sortWith((a, b) => a._1.before(b._1))
    sortedList.last._2._2 - sortedList.head._2._2
  }
}

