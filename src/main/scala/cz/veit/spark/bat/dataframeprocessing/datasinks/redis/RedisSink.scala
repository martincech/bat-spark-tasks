package cz.veit.spark.bat.dataframeprocessing.datasinks.redis

import org.apache.spark.sql.ForeachWriter
import redis.clients.jedis.Jedis

trait RedisSink[T]
  extends ForeachWriter[T]
{
  protected var redis: Jedis = null

  def host: String
  def port: Integer

  override def open(partitionId: Long, version: Long): Boolean = {
    redis = new Jedis(host, port)
    true
  }
  override def close(errorOrNull: Throwable): Unit = {
    redis.close()
  }
}

