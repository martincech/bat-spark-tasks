package cz.veit.spark.bat.dataframeprocessing.aggregations.functions

import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.{Encoder, Encoders, Row}

class Uniformity(valueName: String, uniformityRange: Double = 10)
  extends Aggregator[Row, Array[Double], Double]
    with Serializable {

  override def zero: Array[Double] = Array.empty[Double]

  override def reduce(buf: Array[Double], input: Row): Array[Double] = {
    val v = input.getAs[Double](valueName)
    buf ++ Array(v)
  }

  override def merge(b1: Array[Double], b2: Array[Double]): Array[Double] = {
    b1 ++ b2
  }

  override def finish(b: Array[Double]): Double = {
    val sum = b.sum
    val count = b.length
    if (count <= 1) {
      100.toDouble
    } else {
      val avg: Double = sum / count
      val min: Double = avg - ((uniformityRange / 100) * avg)
      val max: Double = avg + ((uniformityRange / 100) * avg)
      val countInRange: Long = b.count(v => v >= min && v <= max)
      ((100 * countInRange) / count).toDouble
    }
  }

  override def bufferEncoder: Encoder[Array[Double]] = Encoders.kryo[Array[Double]]

  override def outputEncoder: Encoder[Double] = Encoders.scalaDouble
}
