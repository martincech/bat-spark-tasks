package cz.veit.spark.bat.dataframeprocessing.datasinks.influxdb

import cz.veit.spark.bat.dataobjects.topicnames.{DailyTopic, GroupedTopic, RawDataTopic}

object InfluxConstants{
  val rawDataToMeasurement = Map(
    RawDataTopic(RawDataTopic.Temperature) -> "temperature",
    RawDataTopic(RawDataTopic.Weight) -> "weight",
    RawDataTopic(RawDataTopic.Humidity) -> "humidity",
    RawDataTopic(RawDataTopic.Co2) -> "co2",
    DailyTopic(RawDataTopic.Weight.toString) -> "weight")


  def grouppedDataToMeasurement(name: String): String = {
    rawDataToMeasurement(GroupedTopic.strip(name))
  }

}
