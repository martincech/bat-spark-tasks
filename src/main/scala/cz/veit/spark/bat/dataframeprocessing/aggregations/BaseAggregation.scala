package cz.veit.spark.bat.dataframeprocessing.aggregations

import cz.veit.spark.bat.dataframeprocessing.BaseProcessing
import cz.veit.spark.bat.dataframeprocessing.aggregations.functions._
import cz.veit.spark.bat.dataframeprocessing.datasinks.{BaseDataSinkFactory, DataSinkWriter}
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import cz.veit.spark.bat.dataframeprocessing.global.{DataFrameExtensions, TimeConstants}
import cz.veit.spark.bat.dataobjects.AggregationResult
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.{DataFrame, Dataset, RelationalGroupedDataset, SparkSession}

import scala.concurrent.duration._

abstract class BaseAggregation(dataSource: BaseDataSource,
                               dataSink: BaseDataSinkFactory,
                               sparkSession: SparkSession,
                               extendedStats: Boolean = true)
  extends BaseProcessing(dataSource, dataSink, sparkSession) {

  protected class ExtendedDf(dataFrame: DataFrame)
    extends DataFrameExtensions(dataFrame) {



    def watermark(): DataFrame = {
      val watermarkDuration: FiniteDuration = if (extendedStats) TimeConstants.WatermarkDurationLong else TimeConstants.WatermarkDurationShort
      val futureStamp: FiniteDuration = if (extendedStats) TimeConstants.AcceptedFutureStampLong else TimeConstants.AcceptedFutureStampShort
      dataFrame
        .filter(unix_timestamp(col("timestamp")).leq(unix_timestamp(current_timestamp())+lit(futureStamp.toSeconds)))
        .withWatermark("timestamp", watermarkDuration.toString())
    }

    def deduplicate(cols: String*): DataFrame = {
      if (extendedStats)
        dataFrame.dropDuplicates(cols)
      else
        dataFrame
    }
  }

  protected class ExtendedDs(dataset: RelationalGroupedDataset) {
    def aggregate(): DataFrame = {
      import sparkSession.implicits._
      if (extendedStats)
        dataset.agg(
          count($"value") as "count",
          mean($"value") as "average",
          max($"value") as "max",
          min($"value") as "min",
          stddev($"value") as "sigma",
          new Cv("value").toColumn as "cv",
          new Uniformity("value", 10).toColumn as "uniformity"
        )
      else
        dataset.agg(
          count($"value") as "count",
          mean($"value") as "average",
          max($"value") as "max",
          min($"value") as "min")
          .withColumn("sigma", lit(0))
          .withColumn("cv", lit(0))
          .withColumn("uniformity", lit(0))
    }

    def aggregateLast(): DataFrame = {
      if (extendedStats)
        dataset.agg(
          new LastValue("count").toColumn as "count",
          new LastValue("average").toColumn as "average",
          new LastValue("max").toColumn as "max",
          new LastValue("min").toColumn as "min",
          new LastValue("sigma").toColumn as "sigma",
          new LastValue("cv").toColumn as "cv",
          new LastValue("uniformity").toColumn as "uniformity",
          new LastValueDifference("average").toColumn as "gain")
      else
        dataset.agg(
          new LastValue("count").toColumn as "count",
          new LastValue("average").toColumn as "average",
          new LastValue("max").toColumn as "max",
          new LastValue("min").toColumn as "min")
          .withColumn("sigma", lit(0))
          .withColumn("cv", lit(0))
          .withColumn("uniformity", lit(0))
          .withColumn("gain", lit(0))
    }

    def aggregateWeighted(): DataFrame = {
      if (extendedStats)
        dataset.agg(
          new LastSum("count").toColumn as "count",
          new LastWeightedAverage("average", countZeroValues = false).toColumn as "average",
          new Min("min", countZeroValues = false).toColumn as "min",
          max("max") as "max",
          new LastWeightedAverage("sigma").toColumn as "sigma",
          new LastWeightedAverage("cv").toColumn as "cv",
          new LastWeightedAverage("gain", countZeroValues = false).toColumn as "gain",
          new LastWeightedAverage("uniformity").toColumn as "uniformity"
        )
      else
        dataset.agg(
          new LastSum("count").toColumn as "count",
          new LastWeightedAverage("average", countZeroValues = false).toColumn as "average",
          new Min("min", countZeroValues = false).toColumn as "min",
          max("max") as "max"
        )
          .withColumn("sigma", lit(0))
          .withColumn("cv", lit(0))
          .withColumn("uniformity", lit(0))
          .withColumn("gain", lit(0))

    }
  }

  protected object Extensions {
    implicit def dataFrame(df: DataFrame): ExtendedDf = new ExtendedDf(df)

    implicit def relationalGroupedDataset(ds: RelationalGroupedDataset): ExtendedDs = new ExtendedDs(ds)

    implicit def writer(ds: Dataset[AggregationResult]): DataSinkWriter[AggregationResult] = new DataSinkWriter[AggregationResult](ds, dataSink)
  }

  def aggregate(duration: FiniteDuration, descriptor: String): StreamingQuery
}


