package cz.veit.spark.bat.dataframeprocessing.aggregations.functions

import java.sql.Timestamp

import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.{Encoder, Encoders, Row}

class LastValue(valueName: String)
  extends Aggregator[Row, Map[Timestamp, (Timestamp, Double)], Double]
    with Serializable {

  override def zero: Map[Timestamp, (Timestamp, Double)] = Map[Timestamp, (Timestamp, Double)]()

  override def reduce(map: Map[Timestamp, (Timestamp, Double)], input: Row): Map[Timestamp, (Timestamp, Double)] = {
    val timestamp = input.getAs[Timestamp]("timestamp")
    val value = input.getAs[Double](valueName)
    val recordTime =  input.getAs[Timestamp]("recordTime")

    if(map.contains(timestamp) && map(timestamp)._1.after(recordTime)){
      map
    }else {
      map + (timestamp -> (recordTime, value))
    }
  }

  override def merge(map1: Map[Timestamp, (Timestamp, Double)], map2: Map[Timestamp, (Timestamp, Double)]): Map[Timestamp, (Timestamp, Double)] = {
    map1 ++ map2.map(
      item => {
        val v1 = item._2
        val v2 = map1.getOrElse(item._1, v1)
        if(v1._1.after(v2._1)) {
          item._1 ->  v1
        } else {
          item._1 ->  v2
        }
      })
  }

  override def finish(reduction: Map[Timestamp, (Timestamp, Double)]): Double = {
    val map = reduction
    map.toSeq.sortWith((a,b)=>a._1.before(b._1)).last._2._2
  }

  override def bufferEncoder: Encoder[Map[Timestamp, (Timestamp, Double)]] =
    Encoders.kryo[Map[Timestamp, (Timestamp, Double)]]

  override def outputEncoder: Encoder[Double] = Encoders.scalaDouble
}
