package cz.veit.spark.bat.dataframeprocessing.datasources.Kafka

import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.collection.mutable

object KafkaDataSource {
  private var baseFrameObjects: scala.collection.mutable.Map[Seq[String], KafkaDataSource] =
    mutable.Map[Seq[String], KafkaDataSource]()

  def apply(spark: SparkSession, brokers: String, topics: Seq[String]): KafkaDataSource = {
    if (!baseFrameObjects.contains(topics)) {
      baseFrameObjects += topics -> new KafkaDataSource(spark, brokers, topics)
    }
    baseFrameObjects(topics)
  }
  def apply(spark: SparkSession, brokers: String, topic: String): KafkaDataSource = apply(spark,brokers,topic::Nil)
}

final class KafkaDataSource(spark: SparkSession, brokers: String, topics: Seq[String])
  extends BaseDataSource {
  val df: DataFrame = spark
    .readStream
    .format("kafka")
    .option("kafka.bootstrap.servers", brokers)
    .option("subscribe", topics.mkString(","))
    .option("startingOffsets", "earliest")
    .option("failOnDataLoss", false)
    .load()
}










