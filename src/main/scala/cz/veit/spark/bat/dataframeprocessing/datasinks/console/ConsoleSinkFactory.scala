package cz.veit.spark.bat.dataframeprocessing.datasinks.console

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import org.apache.spark.sql.ForeachWriter
import org.apache.spark.sql.streaming.DataStreamWriter

import scala.reflect.runtime.universe.TypeTag

object ConsoleSinkFactory {
  def apply(): ConsoleSinkFactory = {
    new ConsoleSinkFactory()
  }
}

final class ConsoleSinkFactory()
  extends BaseDataSinkFactory {

  override def setupWriter[T: TypeTag](sw: DataStreamWriter[T]): DataStreamWriter[T] = {
    sw.foreach(new ConsoleSink[T]().asInstanceOf[ForeachWriter[T]])
  }
}

