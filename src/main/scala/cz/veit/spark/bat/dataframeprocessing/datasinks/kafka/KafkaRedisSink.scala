package cz.veit.spark.bat.dataframeprocessing.datasinks.kafka

import java.sql.Timestamp

import cz.veit.spark.bat.dataframeprocessing.datasinks.redis.RedisSink
import cz.veit.spark.bat.dataobjects.{Group, GroupEvent}
import cz.veit.spark.bat.serialization.JsonSerializer
import org.apache.spark.sql.ForeachWriter

trait KafkaRedisSink[I, O]
  extends ForeachWriter[I]
    with RedisSink[I]
    with KafkaSink[I, O] {

  def mapToOutput(group: String, database: String, record: I): O

  def timestampForRecord(record: I): Timestamp

  def topicForRecord(record: I): String

  def idForRecord(record: I): String

  def transformedIdForRecord(record: I): String

  def inTimeRange(ts: Timestamp, devGroup: Group): Boolean = {
    val tsMillis = ts.getTime
    val from = if (devGroup.From == null) ts else devGroup.From
    val to = if (devGroup.To == null) ts else devGroup.To
    (tsMillis >= from.getTime) && tsMillis <= to.getTime
  }

  def sendToKafka(group: GroupEvent, record: I): Unit = {
    val devGroups = group.Groups
    val devDatabase = group.Database

    devGroups.foreach(devGroup => {
      if (inTimeRange(timestampForRecord(record), devGroup)) {
        send(
          topicForRecord(record),
          idForRecord(record),
          mapToOutput(devGroup.Group, devDatabase, record))
      }
    })
  }

  override def process(record: I): Unit = {
    val groups = redis.get(idForRecord(record))
    if (groups != null && !groups.isEmpty) {
      val group = JsonSerializer.fromJson[GroupEvent](groups)
      if (group != null)
        sendToKafka(group, record)
    }
  }

  override def open(partitionId: Long, version: Long): Boolean = {
    super[RedisSink].open(partitionId, version)
    super[KafkaSink].open(partitionId, version)
    super.open(partitionId, version)
  }

  override def close(errorOrNull: Throwable): Unit = {
    super[RedisSink].close(errorOrNull)
    super[KafkaSink].close(errorOrNull)
    super.close(errorOrNull)
  }
}
