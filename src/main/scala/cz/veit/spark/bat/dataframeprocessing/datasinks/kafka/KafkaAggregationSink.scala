package cz.veit.spark.bat.dataframeprocessing.datasinks.kafka

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseAggregationSink
import cz.veit.spark.bat.dataobjects.AggregationResult

class KafkaAggregationSink(brokerAddress: String,
                           topicTransform: String => String = s => s,
                           idTransform: String => String = s => s)
  extends BaseAggregationSink
    with KafkaSink[AggregationResult, AggregationResult] {

  override def open(partitionId: Long, version: Long): Boolean = {
    super[KafkaSink].open(partitionId, version)
    super.open(partitionId, version)
  }

  override def close(errorOrNull: Throwable): Unit = {
    super[KafkaSink].close(errorOrNull)
    super.close(errorOrNull)
  }


  def process(record: AggregationResult): Unit = {
    send(
      topicTransform(record.topic),
      idTransform(record.id),
      normalizeRecord(record))
  }

  override def broker: String = brokerAddress
}

