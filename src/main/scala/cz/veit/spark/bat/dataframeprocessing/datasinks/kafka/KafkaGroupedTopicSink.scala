package cz.veit.spark.bat.dataframeprocessing.datasinks.kafka

import java.sql.Timestamp
import cz.veit.spark.bat.dataobjects._

class KafkaGroupedTopicSink(kafkaBroker: String, redisHost: String, redisPort: Integer,
                            topicTransform: String => String = s => s,
                            idTransform: String => String = s => s)
  extends KafkaRedisSink[TransformedRawData, TransformedRawDataGrouped] {


  override def host: String = redisHost

  override def port: Integer = redisPort

  override def broker: String = kafkaBroker

  override def mapToOutput(group: String, database: String, record: TransformedRawData): TransformedRawDataGrouped = {
    TransformedRawDataGrouped(
      database,
      group,
      record.value,
      record.timestamp,
      record.sex,
      record.difference)
  }

  override def timestampForRecord(record: TransformedRawData): Timestamp = {
    record.timestamp
  }

  override def topicForRecord(record: TransformedRawData): String = {
    topicTransform(record.topic)
  }

  override def idForRecord(record: TransformedRawData): String = {
    record.id
  }

  override def transformedIdForRecord(record: TransformedRawData): String = {
    idTransform(record.id)
  }
}
