package cz.veit.spark.bat.dataframeprocessing.transformations

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}

class NewRawDataTransformations(dataSource: BaseDataSource,
                                dataSink: BaseDataSinkFactory,
                                sparkSession: SparkSession)
  extends BaseRawDataTransformations(dataSource, dataSink, sparkSession) {

  private class RawTopicsExtensions(df: DataFrame)
    extends RawTransformationsExtensions(df) {

    def unifyFormat(): DataFrame = {
      df
        .withColumnRenamed("sensoruid", "sensorpacksn")
    }

    def selectDataAndTopic(): DataFrame = {
      selectDataAndTopicNoNesting(StructType(
        StructField("TerminalSn", StringType) ::
          StructField("Data", StructType(
            StructField("TimeStamp", TimestampType) ::
              StructField("Value", DoubleType) ::
              StructField("SensorUid", StringType) ::
              StructField("Sex", StringType) ::
              StructField("Difference", StringType) :: Nil
          )
          ) :: Nil))
    }
  }

  override protected def extensionsFactory(df: DataFrame): RawTransformationsExtensions = {
    new RawTopicsExtensions(df)
  }
}


