package cz.veit.spark.bat.dataframeprocessing.aggregations

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import cz.veit.spark.bat.dataframeprocessing.global.TimeConstants
import cz.veit.spark.bat.dataobjects.AggregationResult
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.StreamingQuery

import scala.concurrent.duration._

class AggregationAggregated(dataSource: BaseDataSource,
                            dataSink: BaseDataSinkFactory,
                            sparkSession: SparkSession,
                            extendedStats: Boolean = true)
  extends BaseAggregation(dataSource, dataSink, sparkSession, extendedStats)
{

  override def aggregate(duration: FiniteDuration, descriptor: String): StreamingQuery = {
    import Extensions._
    import sparkSession.implicits._
    df.selectDataAndTopic[AggregationResult]()
      .withColumnRenamed("intervalStart", "timestamp")
      .watermark()
      .groupBy(
        window($"timestamp", duration.toString()),
        $"datatopic", $"database", $"id", $"sex")
      .aggregateWeighted()
      .drop($"topic")
      .withColumnRenamed("datatopic", "topic")
      .withColumn("intervalStart", $"window.start")
      .withColumn("intervalEnd", $"window.end")
      .as[AggregationResult]
      .start(TimeConstants.AggregationAggregatedTriggerDuration, descriptor, duration)

  }
}
