package cz.veit.spark.bat.dataframeprocessing.aggregations.functions

import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.{Encoder, Encoders, Row}

class Min(valueName: String, countZeroValues: Boolean = true)
  extends Aggregator[Row, Double, Double]
    with Serializable {
  override def zero: Double = 0

  override def reduce(buf: Double, input: Row): Double = {
    val value = input.getAs[Double](valueName)
    merge(value, buf)
  }

  override def merge(v1: Double, v2: Double): Double = {
    if(!countZeroValues){
      if(v1 == 0){
        return v2
      }else if(v2 == 0){
        return v1
      }
    }
    if (v1 < v2) v1 else v2
  }

  override def finish(reduction: Double): Double = {
    reduction
  }

  override def bufferEncoder: Encoder[Double] = Encoders.scalaDouble

  override def outputEncoder: Encoder[Double] = Encoders.scalaDouble
}
