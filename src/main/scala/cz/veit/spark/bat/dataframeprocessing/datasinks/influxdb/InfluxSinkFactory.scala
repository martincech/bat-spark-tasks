package cz.veit.spark.bat.dataframeprocessing.datasinks.influxdb

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataobjects.{AggregationResult, TransformedRawData}
import org.apache.spark.sql.ForeachWriter
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.streaming.DataStreamWriter

import scala.reflect.runtime.universe.TypeTag

object InfluxSinkFactory {

  def apply(address: String): InfluxSinkFactory = {
    new InfluxSinkFactory(address)
  }
}

final class InfluxSinkFactory(address: String)
  extends BaseDataSinkFactory {

  def setupWriter[T: TypeTag](sw: DataStreamWriter[T]): DataStreamWriter[T] = {
    if (ScalaReflection.dataTypeFor[T] == ScalaReflection.dataTypeFor[AggregationResult]) {
      return sw.foreach(new InfluxAggregationSink(address).asInstanceOf[ForeachWriter[T]])
    }
    if (ScalaReflection.dataTypeFor[T] == ScalaReflection.dataTypeFor[TransformedRawData]) {
      return sw.foreach(new InfluxRawDataSink(address).asInstanceOf[ForeachWriter[T]])
    }
    throw new NotImplementedError("Influx sink not implemented for type:" + ScalaReflection.dataTypeFor[T].typeName)
  }
}