package cz.veit.spark.bat.dataframeprocessing.datasinks.influxdb

import java.net.URI

import com.github.fsanaulla.chronicler.urlhttp.io.{InfluxIO, UrlIOClient}
import com.github.fsanaulla.chronicler.urlhttp.shared.InfluxConfig

trait BaseInfluxDataSink {
  def conString: String
  var influxDb: UrlIOClient = _

  def open(partitionId: Long, version: Long): Boolean = {
    // open connection
    val uri = new URI(conString)

    try {
      val conf = InfluxConfig(uri.getHost, uri.getPort)
      influxDb = InfluxIO(conf)
    } catch {
      case _: Exception =>
        closeIfNeccessary()
        return false
    }
    true
  }

  private def closeIfNeccessary(): Unit = {
    if (influxDb != null){
      influxDb.close()
    }
    influxDb = null
  }

  def close(errorOrNull: Throwable): Unit = {
    closeIfNeccessary()
  }

}
