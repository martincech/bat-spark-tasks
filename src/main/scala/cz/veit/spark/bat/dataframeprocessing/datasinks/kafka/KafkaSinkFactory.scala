package cz.veit.spark.bat.dataframeprocessing.datasinks.kafka

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataobjects.{AggregationResult, DailyWeightAggregation, TransformedRawData}
import org.apache.spark.sql.ForeachWriter
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.streaming.DataStreamWriter

import scala.reflect.runtime.universe.TypeTag

object KafkaSinkFactory {
  def apply(brokers: String,
            redisHost: String,
            redisPort: Integer,
            topicTransform: String => String = s => s,
            idTransform: String => String = s => s): KafkaSinkFactory = {
    new KafkaSinkFactory(brokers, topicTransform, idTransform, redisHost, redisPort)
  }
}

final class KafkaSinkFactory(brokers: String,
                             topicTransform: String => String = s => s,
                             idTransform: String => String = s => s,
                             redisHost: String = null,
                             redisPort: Integer = 0)
  extends BaseDataSinkFactory {

  def setupWriter[T: TypeTag](sw: DataStreamWriter[T]): DataStreamWriter[T] = {
    if (ScalaReflection.dataTypeFor[T] == ScalaReflection.dataTypeFor[AggregationResult]) {
      return sw.foreach(new KafkaAggregationSink(brokers, topicTransform, idTransform).asInstanceOf[ForeachWriter[T]])
    }
    if (ScalaReflection.dataTypeFor[T] == ScalaReflection.dataTypeFor[DailyWeightAggregation]) {
      return sw.foreach(new KafkaGroupedDailyWeightSink(brokers, redisHost, redisPort, topicTransform, idTransform).asInstanceOf[ForeachWriter[T]])
    }
    if (ScalaReflection.dataTypeFor[T] == ScalaReflection.dataTypeFor[TransformedRawData]) {
      return sw.foreach(new KafkaGroupedTopicSink(brokers, redisHost, redisPort, topicTransform, idTransform).asInstanceOf[ForeachWriter[T]])
    }
    throw new NotImplementedError("Kafka sink not implemented for type:" + ScalaReflection.dataTypeFor[T].typeName)
  }
}