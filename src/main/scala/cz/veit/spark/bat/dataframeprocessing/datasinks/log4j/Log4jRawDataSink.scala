package cz.veit.spark.bat.dataframeprocessing.datasinks.log4j

import cz.veit.spark.bat.dataobjects.TransformedRawData
import org.apache.log4j.Logger


class Log4jRawDataSink()
  extends Log4jDataSink[TransformedRawData] {
  override def logger: Logger = {
    Logger.getLogger(classOf[Log4jRawDataSink])
  }
}

