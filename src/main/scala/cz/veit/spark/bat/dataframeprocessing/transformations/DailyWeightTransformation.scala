package cz.veit.spark.bat.dataframeprocessing.transformations

import cz.veit.spark.bat.dataframeprocessing.datasinks.{BaseDataSinkFactory, DataSinkWriter}
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import cz.veit.spark.bat.dataframeprocessing.global.{DataFrameExtensions, TimeConstants}
import cz.veit.spark.bat.dataobjects.DailyWeightAggregation
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

import scala.concurrent.duration._

class DailyWeightTransformation(dataSource: BaseDataSource,
                                dataSink: BaseDataSinkFactory,
                                sparkSession: SparkSession)
  extends BaseTransformation(dataSource, dataSink, sparkSession) {

  private class DailyWeightExtensions(df: DataFrame)
    extends DataFrameExtensions(df) {

    def filterMalformedData(): DataFrame = {
      df.filter(
        col("uid").isNotNull && col("uid") =!= ""
      )
    }

    def selectDataAndTopic(): DataFrame = {
      toLower(selectDataAndTopic[DailyWeightAggregation]())
    }
  }

  private class DailyWeightDsExtensions(ds: Dataset[DailyWeightAggregation]) {
    def mapToGrams(): Dataset[DailyWeightAggregation] = {
      import sparkSession.implicits._
      ds.map(row => {
        def toGrams(value: Double): Double = {
//          if (row.Average < 50) value * 1000 else value
          value
        }

        if (row.Gain == row.Average) {
          DailyWeightAggregation(
            row.Uid,
            row.TimeStamp,
            row.Sex,
            row.Count,
            row.Day,
            toGrams(row.Average),
            0,
            toGrams(row.Sigma),
            row.Cv,
            row.Uniformity)
        } else {
          DailyWeightAggregation(
            row.Uid,
            row.TimeStamp,
            row.Sex,
            row.Count,
            row.Day,
            toGrams(row.Average),
            toGrams(row.Gain),
            toGrams(row.Sigma),
            row.Cv,
            row.Uniformity)
        }
      })
    }
  }

  private object Extensions {
    implicit def dataFrame(df: DataFrame): DailyWeightExtensions = new DailyWeightExtensions(df)

    implicit def dataSet(ds: Dataset[DailyWeightAggregation]): DailyWeightDsExtensions = new DailyWeightDsExtensions(ds)

    implicit def writer(ds: Dataset[DailyWeightAggregation]): DataSinkWriter[DailyWeightAggregation] = new DataSinkWriter[DailyWeightAggregation](ds, dataSink)
  }

  def transform(descriptor: String): StreamingQuery = {
    import Extensions._
    import sparkSession.implicits._

    df.selectDataAndTopic()
      .filterMalformedData()
      .as[DailyWeightAggregation]
      .mapToGrams()
      .start(TimeConstants.DailyWeightTransformationTriggerDuration, descriptor)
  }
}
