package cz.veit.spark.bat.dataframeprocessing.global

object HDFSStore {
  var hdfs: String = ""

  def hdfsPath = if (hdfs.isEmpty) "" else "hdfs://" + hdfs + "/"

}
