package cz.veit.spark.bat.dataframeprocessing.datasinks.log4j

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataobjects.{AggregationResult, DailyWeightAggregation, TransformedRawData}
import org.apache.spark.sql.ForeachWriter
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.streaming.DataStreamWriter

import scala.reflect.runtime.universe.TypeTag

object Log4jSinkFactory {

  def apply(): Log4jSinkFactory = {
    new Log4jSinkFactory()
  }
}

final class Log4jSinkFactory()
  extends BaseDataSinkFactory {

  def setupWriter[T: TypeTag](sw: DataStreamWriter[T]): DataStreamWriter[T] = {
    if (ScalaReflection.dataTypeFor[T] == ScalaReflection.dataTypeFor[TransformedRawData]) {
      return sw.foreach(new Log4jRawDataSink().asInstanceOf[ForeachWriter[T]])
    }
    if (ScalaReflection.dataTypeFor[T] == ScalaReflection.dataTypeFor[DailyWeightAggregation]) {
      return sw.foreach(new Log4jDailyWeightAggregationSink().asInstanceOf[ForeachWriter[T]])
    }

    if (ScalaReflection.dataTypeFor[T] == ScalaReflection.dataTypeFor[AggregationResult]) {
      return sw.foreach(new Log4jAggregationSink().asInstanceOf[ForeachWriter[T]])
    }

    throw new NotImplementedError("Log4j sink not implemented for type:" + ScalaReflection.dataTypeFor[T].typeName)
  }
}
