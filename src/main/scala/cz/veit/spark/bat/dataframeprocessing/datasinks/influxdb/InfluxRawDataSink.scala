package cz.veit.spark.bat.dataframeprocessing.datasinks.influxdb

import com.github.fsanaulla.chronicler.core.enums.{Consistencies, Precisions}
import com.github.fsanaulla.chronicler.core.model.InfluxFormatter
import com.github.fsanaulla.chronicler.macros.Influx
import com.github.fsanaulla.chronicler.macros.annotations.{field, tag, timestamp}
import cz.veit.spark.bat.dataobjects.TransformedRawData
import org.apache.log4j.LogManager
import org.apache.spark.sql.ForeachWriter
import org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp
import org.joda.time.DateTime

import scala.util.{Failure, Success}


class InfluxRawDataSink(address: String)
  extends ForeachWriter[TransformedRawData]
  with BaseInfluxDataSink {
  override def conString: String = address
  final case class Point(
      @tag deviceid: String,
      @tag sex: String,
      @field value: Double,
      @field difference: String,
      @timestamp timestamp: Long)
  implicit val fmt: InfluxFormatter[Point] = Influx.formatter[Point]

  private val influxDatabase = "samples"

  def createPoint(record: TransformedRawData): Point = {

    var sex = ""
    var difference = ""
    try {
      if (record.sex != null && !record.sex.isEmpty) {
        sex =  record.sex
      }
    } catch {
      case e: IllegalArgumentException =>
    }
    try {
      if (record.difference != null && !record.difference.isEmpty) {
        difference = record.difference
      }
    } catch {
      case e: IllegalArgumentException =>
    }
    val timestamp = new DateTime(record.timestamp).getMillis
    Point(
      record.id,
      sex,
      record.value,
      difference,
      timestamp)
  }

  def process(record: TransformedRawData): Unit = {
      val retentionPolicy = "raw"

      val measName = InfluxConstants.rawDataToMeasurement(record.topic)
      val dbName = influxDatabase
      val meas = influxDb.measurement[Point](dbName, measName)
      val value = createPoint(record)
      meas.write(value,
        Option(Consistencies.ONE),
        Option(Precisions.MILLISECONDS),
        Option(retentionPolicy))
      match {
        case Success(v)  => LogManager.getRootLogger.info("Successfully pushed data to influx: " + v)
        case Failure(ex) => throw ex
      }
  }
}
