package cz.veit.spark.bat.dataframeprocessing.datasinks.log4j

import cz.veit.spark.bat.dataobjects.AggregationResult
import org.apache.log4j.Logger

class Log4jAggregationSink()
  extends Log4jDataSink[AggregationResult] {
  override def logger: Logger = {
    Logger.getLogger(classOf[Log4jAggregationSink])
  }
}
