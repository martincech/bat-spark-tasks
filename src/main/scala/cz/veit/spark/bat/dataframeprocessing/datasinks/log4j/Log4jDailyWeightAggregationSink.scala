package cz.veit.spark.bat.dataframeprocessing.datasinks.log4j

import cz.veit.spark.bat.dataobjects.DailyWeightAggregation
import org.apache.log4j.Logger

class Log4jDailyWeightAggregationSink()
  extends Log4jDataSink[DailyWeightAggregation] {
  override def logger: Logger = {
    Logger.getLogger(classOf[Log4jDailyWeightAggregationSink])
  }
}
