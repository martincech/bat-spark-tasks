package cz.veit.spark.bat.dataframeprocessing.aggregations

import cz.veit.spark.bat.dataframeprocessing.datasinks.BaseDataSinkFactory
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import cz.veit.spark.bat.dataframeprocessing.global.TimeConstants
import cz.veit.spark.bat.dataobjects.AggregationResult
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, from_unixtime, unix_timestamp, window}
import org.apache.spark.sql.streaming.StreamingQuery

import scala.concurrent.duration._

class AggregationWithGain(baseDataFrame: BaseDataSource,
                          dataSink: BaseDataSinkFactory,
                          sparkSession: SparkSession,
                          extendedStats: Boolean = true)
  extends BaseAggregation(baseDataFrame, dataSink, sparkSession, extendedStats) {

  def aggregate(duration: FiniteDuration, descriptor: String): StreamingQuery = {
    import Extensions._
    import sparkSession.implicits._
    df.selectDataAndTopic[AggregationResult]()
      .withColumnRenamed("intervalStart", "timestamp")
      .watermark()
      .groupBy(
        window($"timestamp", (duration * 2).toString(), duration.toString()),
        $"datatopic", $"database", $"id", $"sex")
      .aggregateLast()
      .drop($"topic")
      .withColumnRenamed("datatopic", "topic")
      .withColumn("intervalStart", from_unixtime(unix_timestamp(col("window.end")) - duration.toSeconds))
      .withColumn("intervalEnd", $"window.end")
      .as[AggregationResult]
      .start(TimeConstants.AggregationWithGainTriggerDuration, descriptor, duration)
  }
}
