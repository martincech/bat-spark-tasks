package cz.veit.spark.bat.dataframeprocessing.aggregations.functions

import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.{Encoder, Encoders, Row}

class Cv(valueName: String)
  extends Aggregator[Row, CvBuffer, Double]
    with Serializable {

  override def zero: CvBuffer = CvBuffer(0.0, 0.0, 0l)

  override def reduce(buffer: CvBuffer, input: Row): CvBuffer = {
    val x = input.getAs[Double](valueName)
    CvBuffer(buffer.xSum + x, buffer.x2Sum + math.pow(x, 2), buffer.count + 1)
  }

  override def merge(b1: CvBuffer, b2: CvBuffer): CvBuffer = {
    CvBuffer(b1.xSum + b2.xSum, b1.x2Sum + b2.x2Sum, b1.count + b2.count)
  }

  override def finish(buf: CvBuffer): Double = {
    if (buf.count <= 1) {
      0.toDouble
    } else {
      val avg = buf.xSum / buf.count.toDouble
      val stddev = math.sqrt((buf.x2Sum - (math.pow(buf.xSum, 2.0) / buf.count.toDouble)) / (buf.count.toDouble - 1))
      stddev / avg * 100
    }
  }

  override def bufferEncoder: Encoder[CvBuffer] = Encoders.kryo[CvBuffer]

  override def outputEncoder: Encoder[Double] = Encoders.scalaDouble
}
