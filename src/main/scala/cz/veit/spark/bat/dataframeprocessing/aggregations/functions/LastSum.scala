package cz.veit.spark.bat.dataframeprocessing.aggregations.functions

import java.sql.Timestamp

import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.{Encoder, Encoders, Row}

class LastSum(valueName: String)
  extends Aggregator[Row, Map[String, (Timestamp, Double, Timestamp)], Double]
    with Serializable {

  private def getNewest(oldVal: (Timestamp, Double, Timestamp), newVal: (Timestamp, Double, Timestamp)): (Timestamp, Double, Timestamp) = {
    if (oldVal._1.after(newVal._1) || (oldVal._1 == newVal._1 && oldVal._3.after(newVal._3))) oldVal
    else newVal
  }

  override def zero: Map[String, (Timestamp, Double, Timestamp)] = Map[String, (Timestamp, Double, Timestamp)]()

  override def reduce(map: Map[String, (Timestamp, Double, Timestamp)], input: Row): Map[String, (Timestamp, Double, Timestamp)] = {
    val key = input.getAs[String]("key")
    val timestamp = input.getAs[Timestamp]("timestamp")
    val value = input.getAs[Double](valueName)
    val recordTime =  input.getAs[Timestamp]("recordTime")

    val newVal = (timestamp, value, recordTime)
    val oldVal = map.getOrElse(key, newVal)

    map + (key -> getNewest(oldVal, newVal))
  }

  override def merge(map1: Map[String, (Timestamp, Double, Timestamp)], map2: Map[String, (Timestamp, Double, Timestamp)]): Map[String, (Timestamp, Double, Timestamp)] = {
    map1 ++ map2.map(
      item => {
        val newV = item._2
        val oldV = map1.getOrElse(item._1, newV)
        item._1 -> getNewest(oldV, newV)
      })
  }

  override def finish(map: Map[String, (Timestamp, Double, Timestamp)]): Double = {
    map.foldLeft(0.0)((a, b) => a + b._2._2)
  }

  override def bufferEncoder: Encoder[Map[String, (Timestamp, Double, Timestamp)]] =
    Encoders.kryo[Map[String, (Timestamp, Double, Timestamp)]]

  override def outputEncoder: Encoder[Double] = Encoders.scalaDouble
}

