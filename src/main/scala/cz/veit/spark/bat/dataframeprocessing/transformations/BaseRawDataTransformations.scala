package cz.veit.spark.bat.dataframeprocessing.transformations

import cz.veit.spark.bat.dataframeprocessing.datasinks.{BaseDataSinkFactory, DataSinkWriter}
import cz.veit.spark.bat.dataframeprocessing.datasources.BaseDataSource
import cz.veit.spark.bat.dataframeprocessing.global.{DataFrameExtensions, TimeConstants}
import cz.veit.spark.bat.dataobjects.TransformedRawData
import org.apache.spark.sql.functions.{col, concat, lit}
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

import scala.concurrent.duration._

abstract class BaseRawDataTransformations(dataSource: BaseDataSource,
                                          dataSink: BaseDataSinkFactory,
                                          sparkSession: SparkSession)
  extends BaseTransformation(dataSource, dataSink, sparkSession) {

  protected abstract class RawTransformationsExtensions(df: DataFrame) extends DataFrameExtensions(df) {
    def selectDataAndTopicNoNesting(dataStruct: StructType): DataFrame = {
      toLower(selectDataAndTopic(dataStruct)
        .select(col("*"), col("Data.*"))
        .drop(col("Data")))
    }

    def filterMalformedData(): DataFrame = {
      df.filter(
        col("terminalsn").isNotNull && col("terminalsn") =!= ""
          && col("sensorpacksn").isNotNull && col("sensorpacksn") =!= ""
          && col("timestamp").isNotNull
          && col("value").isNotNull && !col("value").isNaN
      )
    }

    def createIdColumn(): DataFrame = {
      df.withColumn("id", concat(col("sensorpacksn"), lit("-T-"), col("terminalsn")))
    }

    def unifyFormat(): DataFrame

    def selectDataAndTopic(): DataFrame
  }

  private object Extensions {
    implicit def dataSet(ds: DataFrame): RawTransformationsExtensions = extensionsFactory(ds)

    implicit def writer(ds: Dataset[TransformedRawData]): DataSinkWriter[TransformedRawData] = new DataSinkWriter[TransformedRawData](ds, dataSink)
  }

  protected def extensionsFactory(df: DataFrame): RawTransformationsExtensions

  override def transform(descriptor: String): StreamingQuery = {
    import Extensions._
    import sparkSession.implicits._
    df.selectDataAndTopic()
      .unifyFormat()
      .filterMalformedData()
      .createIdColumn()
      .as[TransformedRawData]
      .start(TimeConstants.RawDataTransformationTriggerDuration, descriptor)
  }
}
