package cz.veit.spark.bat.dataframeprocessing.datasinks.console

import cz.veit.spark.bat.serialization.JsonSerializer
import org.apache.log4j.Logger
import org.apache.spark.sql.ForeachWriter

class ConsoleSink[T]() extends ForeachWriter[T] {
  override def open(partitionId: Long, version: Long): Boolean = {
    true
  }

  def logger: Logger = Logger.getLogger(classOf[ConsoleSink[T]])

  override def process(value: T): Unit = {
    logger.info(JsonSerializer.toJson(value))
  }

  override def close(errorOrNull: Throwable): Unit = {

  }
}
