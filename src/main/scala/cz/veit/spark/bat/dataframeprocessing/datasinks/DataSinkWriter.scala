package cz.veit.spark.bat.dataframeprocessing.datasinks

import cz.veit.spark.bat.dataframeprocessing.global.Descriptors
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.streaming.{OutputMode, StreamingQuery, Trigger}
import scala.reflect.runtime.universe.TypeTag
import scala.concurrent.duration.FiniteDuration

final class DataSinkWriter[T: TypeTag](ds: Dataset[T], factory: BaseDataSinkFactory) {
  def start(trigger: FiniteDuration, descriptor: String, interval: FiniteDuration = null)
  : StreamingQuery = {
    val dsWriter = ds.writeStream
      .outputMode(OutputMode.Update)
      .option("checkpointLocation", Descriptors.newCheckpointName(
        Descriptors.unifyDescriptor(descriptor, interval)
      ))
      .queryName(
        Descriptors.unifyDescriptor(descriptor, interval))
      .trigger(Trigger.ProcessingTime(trigger))

    factory
      .setupWriter(dsWriter)
      .start()
  }
}
