package cz.veit.spark.bat.dataframeprocessing.datasinks.redis

import cz.veit.spark.bat.dataobjects.GroupEvent
import cz.veit.spark.bat.serialization.JsonSerializer

class GroupRedisSink(redisHost: String, redisPort: Integer)
  extends RedisSink[GroupEvent] {
  override def process(value: GroupEvent): Unit = {
    redis.set(value.Uid, JsonSerializer.toJson(value))
  }

  override def host: String = redisHost

  override def port: Integer = redisPort
}
