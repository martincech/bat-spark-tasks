package cz.veit.spark.bat.dataframeprocessing.datasinks

import org.apache.spark.sql.streaming.DataStreamWriter
import scala.reflect.runtime.universe.TypeTag

trait BaseDataSinkFactory extends Serializable {
  def setupWriter[T: TypeTag](sw: DataStreamWriter[T]): DataStreamWriter[T]
}