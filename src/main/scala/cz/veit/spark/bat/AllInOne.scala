package cz.veit.spark.bat

import java.sql.Timestamp
import java.util.{Timer, TimerTask, UUID}

import com.github.fsanaulla.chronicler.urlhttp.io.UrlIOClient
import cz.veit.spark.bat.dataframeprocessing.aggregations._
import cz.veit.spark.bat.dataframeprocessing.aggregations.functions._
import cz.veit.spark.bat.dataframeprocessing.datasinks.influxdb.{InfluxAggregationSink, InfluxRawDataSink, InfluxSinkFactory}
import cz.veit.spark.bat.dataframeprocessing.datasinks.kafka.{KafkaAggregationSink, KafkaGroupedDailyWeightSink, KafkaGroupedTopicSink, KafkaSinkFactory}
import cz.veit.spark.bat.dataframeprocessing.datasinks.log4j.{Log4jDailyWeightAggregationSink, Log4jRawDataSink, Log4jSinkFactory}
import cz.veit.spark.bat.dataframeprocessing.datasinks.redis.{GroupRedisSink, RedisSinkFactory}
import cz.veit.spark.bat.dataframeprocessing.datasources.Kafka.KafkaDataSource
import cz.veit.spark.bat.dataframeprocessing.global.{HDFSStore, TimeConstants}
import cz.veit.spark.bat.dataframeprocessing.transformations.{DailyWeightTransformation, GroupTopicTransformations}
import cz.veit.spark.bat.dataobjects.topicnames.{DailyTopic, GroupedTopic, RawDataTopic}
import cz.veit.spark.bat.dataobjects.{AggregationResult, DailyWeightAggregation, _}
import org.apache.log4j.LogManager
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.StreamingQueryListener.{QueryProgressEvent, QueryStartedEvent, QueryTerminatedEvent}
import org.apache.spark.sql.streaming.{StreamingQuery, StreamingQueryListener}
import org.rogach.scallop.ScallopOption

object Defaults {
  val appName = "Spark processing"
  val kafkaBroker = "localhost:9092"
  val sparkMaster = "local[*]"
  val influxDb = "localhost:8086"
  val redisPort = "6379"
  val redisHost = "localhost"
  val redis: String = redisHost + ":" + redisPort
  val hdfs = ""
}

class ProgramArgs(arguments: Seq[String]) extends VeitCommandLine(Defaults.appName, arguments) {
  val info: String = "Usage: " + Defaults.appName + " [OPTION]" +
    "| Transform data from Kafka to Influx" +
    "| Options:" +
    "|"
  banner(info.stripMargin)
  val kafka: ScallopOption[String] = opt[String]("kafka", descr = "Kafka broker list separated by , (server:port,server2:port2)", required = true)
  val influx: ScallopOption[String] = opt[String]("influx", descr = "Influx database server and port (server:port)", required = true)
  val redis: ScallopOption[String] = opt[String]("redis", descr = "Redis server and port (server:port)", required = true, default = Some(Defaults.redis))
  val hdfs: ScallopOption[String] = opt[String]("hdfs", descr = "hdfs namenode server and port (server:port)", required = true, default = Some(Defaults.hdfs))
  verify()
}

object Program extends BaseSparkApp {
  def registerSerializers(): SparkConf = {
    conf.registerKryoClasses((
      classOf[AggregationResult] ::
        classOf[DailyWeightAggregation] ::
        classOf[Group] ::
        classOf[GroupEvent] ::
        classOf[TransformedRawData] ::
        classOf[TransformedRawDataGrouped] ::

        classOf[Cv] ::
        classOf[CvBuffer] ::
        classOf[GaussUniformity] ::
        classOf[LastSum] ::
        classOf[LastValue] ::
        classOf[LastValueDifference] ::
        classOf[LastWeightedAverage] ::
        classOf[Min] ::
        classOf[Uniformity] ::

        classOf[Map[String, (Timestamp, Double, Timestamp)]] ::
        classOf[Map[Timestamp, (Timestamp, Double)]] ::
        classOf[Map[String, (Timestamp, Double, Double, Timestamp)]] ::
        classOf[(Double, Double, Double)] ::
        classOf[Array[Double]] ::

        classOf[KafkaGroupedDailyWeightSink] ::
        classOf[KafkaGroupedTopicSink] ::
        classOf[KafkaAggregationSink] ::
        classOf[InfluxAggregationSink] ::
        classOf[InfluxRawDataSink] ::
        classOf[GroupRedisSink] ::
        classOf[Log4jDailyWeightAggregationSink] ::
        classOf[Log4jRawDataSink] ::

        classOf[UrlIOClient]::
        Nil)
      .toArray)
  }

  val clArgs = new ProgramArgs(args)
  val redisUrl = clArgs.redis().split(":")
  val redisHost = if (redisUrl.nonEmpty) redisUrl(0) else Defaults.redisHost
  val redisPort = if (redisUrl.length > 1) redisUrl(1) else Defaults.redisPort
  val conf = new SparkConf()
    .setIfMissing("spark.master", Defaults.sparkMaster)
    .set("spark.app.name", Defaults.appName)
    .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    .set("spark.sql.streaming.metricsEnabled", "true")
  val spark: SparkSession = {
    SparkSession.builder()
      .config(registerSerializers())
      .getOrCreate()
  }
  val sc = spark.sparkContext
  val existingQueries = scala.collection.mutable.Map[UUID, () => StreamingQuery]()

  HDFSStore.hdfs = clArgs.hdfs()

  def startQueryInNewThread(queryFunc: () => StreamingQuery): Unit = {
    new Thread {
      override def run(): Unit = {
        val streamingQuery = queryFunc()
        existingQueries(streamingQuery.id) = queryFunc
      }
    }
      .start()
  }

  class Listener extends StreamingQueryListener {
    override def onQueryStarted(q: QueryStartedEvent): Unit = {
      LogManager.getRootLogger.info("Query started: " + q.name + "(Active queries: " + spark.streams.active.length + ")")

    }

    override def onQueryTerminated(q: QueryTerminatedEvent): Unit = {
      LogManager.getRootLogger.error("Starting query again")
//      LogManager.getRootLogger.error("Query terminated: " + spark.streams.get(q.id).name + "(Active queries: " + spark.streams.active.length + ")")
      val timer = new Timer
      val delayedThreadStartTask = new TimerTask() {
        override def run(): Unit = {
          val newT = existingQueries(q.id)
          existingQueries.remove(q.id)
          startQueryInNewThread(newT)
        }
      }
      timer.schedule(delayedThreadStartTask, TimeConstants.QueryRestartDelay.toMillis)
//      LogManager.getRootLogger.warn("Scheduled restart of query: " + spark.streams.get(q.id).name)
    }

    override def onQueryProgress(q: QueryProgressEvent): Unit = {
    }

  }

  var queries =
    (() => {
      /*-----------------------------------------------------------
      * Groups to Redis
      * -------------------------------------------------------------*/
      sc.setLocalProperty("spark.scheduler.pool", "highestPrioPool")
      new GroupTopicTransformations(
        KafkaDataSource(spark, clArgs.kafka(), GroupedTopic.Group.toString),
        RedisSinkFactory(redisHost, redisPort.toInt),
        spark
      ).transform("Group To Redis Sink")
    }) ::
      /*-----------------------------------------------------------
      * Transformations
      * -------------------------------------------------------------*/
//      (() => {
//        sc.setLocalProperty("spark.scheduler.pool", "highPrioPool")
//        // raw data topics back to kafka group topics
//        new NewRawDataTransformations(
//          KafkaDataSource(spark, clArgs.kafka(), RawDataTopic.AllTopics),
//          KafkaSinkFactory(clArgs.kafka(), redisHost, redisPort.toInt, x => GroupedTopic(x.toString)),
//          spark
//        ).transform("Raw Data To Grouped")
//      }) ::
      (() => {
        sc.setLocalProperty("spark.scheduler.pool", "highPrioPool")
        // daily weight to grouped daily weight transformation
        new DailyWeightTransformation(
          KafkaDataSource(spark, clArgs.kafka(), DailyTopic.DailyWeight.toString),
          KafkaSinkFactory(clArgs.kafka(), redisHost, redisPort.toInt, x => GroupedTopic(DailyTopic(x)), x => x + "/2"),
          spark
        ).transform("Daily Weight To Grouped")
      }) ::
      (() => {
        sc.setLocalProperty("spark.scheduler.pool", "lowestPrioPool")
        // daily weight to grouped daily weight transformation
        new DailyWeightTransformation(
          KafkaDataSource(spark, clArgs.kafka(), DailyTopic.DailyWeight.toString),
          Log4jSinkFactory(),
          spark
        ).transform("Daily Weight To Log4j")
      }) ::
//      (() => {
//        sc.setLocalProperty("spark.scheduler.pool", "lowestPrioPool")
//        // raw data topics to influx
//        new NewRawDataTransformations(
//          KafkaDataSource(spark, clArgs.kafka(), RawDataTopic.AllTopics),
//          Log4jSinkFactory(),
//          spark
//        ).transform("Raw Data To Log4j")
//      }) ::
//      (() => {
//        sc.setLocalProperty("spark.scheduler.pool", "lowestPrioPool")
//        // raw data topics to influx
//        new NewRawDataTransformations(
//          KafkaDataSource(spark, clArgs.kafka(), RawDataTopic.AllTopics),
//          InfluxSinkFactory(clArgs.influx()),
//          spark
//        ).transform("Raw Data To Influx")
//      }) ::
      /*---------------------------------------------------------------------------
      * Aggregations without gain for - 1 day/30 minutes
      * ---------------------------------------------------------------------------*/
//      (() => {
//        sc.setLocalProperty("spark.scheduler.pool", "lowPrioPool")
//        new AggregationWithoutGain(
//          KafkaDataSource(spark, clArgs.kafka(), RawDataTopic.AllTopicsExceptWeight.map(t => GroupedTopic(t))),
//          //          Log4jSinkFactory(),
//          InfluxSinkFactory(clArgs.influx()),
//          spark,
//          false
//        ).aggregate(TimeConstants.AggregationWindowLong, "Raw Samples Aggregation")
//      }) ::
//      (() => {
//        sc.setLocalProperty("spark.scheduler.pool", "lowPrioPool")
//        new AggregationWithoutGain(
//          KafkaDataSource(spark, clArgs.kafka(), RawDataTopic.AllTopicsExceptWeight.map(t => GroupedTopic(t))),
//          InfluxSinkFactory(clArgs.influx()),
//          spark,
//          false
//        ).aggregate(TimeConstants.AggregationWindowShort, "Raw Samples Aggregation")
//      }) ::
//      (() => {
//        sc.setLocalProperty("spark.scheduler.pool", "lowPrioPool")
//        new AggregationWithoutGain(
//          KafkaDataSource(spark, clArgs.kafka(), (RawDataTopic.Weight.toString :: Nil).map(t => GroupedTopic(t))),
//          KafkaSinkFactory(clArgs.kafka(), redisHost, redisPort.toInt, x => NoGainTopic(x, TimeConstants.AggregationWindowLong)),
//          spark,
//          true
//        ).aggregate(TimeConstants.AggregationWindowLong, "Raw BW Aggregation Intermediate")
//      }) ::
//      (() => {
//        sc.setLocalProperty("spark.scheduler.pool", "lowPrioPool")
//        new AggregationWithoutGain(
//          KafkaDataSource(spark, clArgs.kafka(), (RawDataTopic.Weight.toString :: Nil).map(t => GroupedTopic(t))),
//          KafkaSinkFactory(clArgs.kafka(), redisHost, redisPort.toInt, x => NoGainTopic(x, TimeConstants.AggregationWindowShort)),
//          spark,
//          true
//        ).aggregate(TimeConstants.AggregationWindowShort, "Raw BW Aggregation Intermediate")
//      }) ::
      /*---------------------------------------------------------------------------
      * Aggregations with gain for - 1 day/30 minutes bird
      * ---------------------------------------------------------------------------*/
//      (() => {
//        sc.setLocalProperty("spark.scheduler.pool", "lowPrioPool")
//        // 1 day BirdWeight back to Kafka
//        new AggregationWithGain(
//          KafkaDataSource(spark, clArgs.kafka(), NoGainTopic(GroupedTopic(RawDataTopic.Weight.toString), TimeConstants.AggregationWindowLong)),
//          KafkaSinkFactory(clArgs.kafka(), redisHost, redisPort.toInt, x => GroupedTopic(DailyTopic(GroupedTopic.strip(x))), x => x + "/1"),
//          spark,
//          true)
//          .aggregate(TimeConstants.AggregationWindowLong, "Raw BW Aggregation")
//      }) ::
//      (() => {
//        sc.setLocalProperty("spark.scheduler.pool", "lowPrioPool")
//        // 30 minutes to Influx
//        new AggregationWithGain(
//          KafkaDataSource(spark, clArgs.kafka(), NoGainTopic(GroupedTopic(RawDataTopic.Weight.toString), TimeConstants.AggregationWindowShort)),
//          InfluxSinkFactory(clArgs.influx()),
//          spark,
//          true)
//          .aggregate(TimeConstants.AggregationWindowShort, "Raw BW Aggregation")
//      }) ::
      /*---------------------------------------------------------------------------
      * Aggregations of aggregated data (from SMS and from raw)
      * ---------------------------------------------------------------------------*/
      (() => {
        sc.setLocalProperty("spark.scheduler.pool", "lowPrioPool")
        // 1 day BirdWeight aggregation of aggregated to Influx
        new AggregationAggregated(
          KafkaDataSource(spark, clArgs.kafka(), GroupedTopic(DailyTopic(RawDataTopic.Weight.toString))),
          InfluxSinkFactory(clArgs.influx()),
          spark,
          true
        ).aggregate(TimeConstants.AggregationWindowLong, "Daily Bird Weight Aggregation of Aggregated")
      }) ::
      Nil

  // add listener to start queries if they terminate
  spark.streams.addListener(new Listener)
  // start queries in separate threads
  queries.foreach(queryFunc => startQueryInNewThread(queryFunc))

  //watch query termination and continue
  while (true) {
    try {
      spark.streams.awaitAnyTermination()
    } catch {
      case e: Throwable =>
        LogManager.getRootLogger.error("Exception in driver: " + e.getMessage)
        spark.streams.resetTerminated()
    }
  }
}



