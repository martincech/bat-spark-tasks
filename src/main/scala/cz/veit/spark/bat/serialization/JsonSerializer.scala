package cz.veit.spark.bat.serialization

import java.lang.reflect.Type
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.{Date, TimeZone}
import com.google.gson._
import org.apache.log4j.LogManager
import scala.reflect.ClassTag
import scala.reflect._

object JsonSerializer {
  private val timeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

  private class GsonUTCDateAdapter()
    extends JsonSerializer[Timestamp]
      with JsonDeserializer[Timestamp] {

    def dateFormater(): SimpleDateFormat ={
      val dateFormat = new SimpleDateFormat(JsonSerializer.timeFormat)
      dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
      dateFormat
    }

    override def serialize(date: Timestamp, typeOfSrc: Type, context: JsonSerializationContext): JsonElement = {
      new JsonPrimitive(dateFormater().format(date))
    }

    override def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Timestamp = {
      var dateAsString: String = ""
      var parsed: Date = null
      dateAsString = json.getAsString
      parsed = dateFormater().parse(dateAsString)
      new Timestamp(parsed.getTime)
    }
  }

  private val gson: Gson = new GsonBuilder().registerTypeAdapter(classOf[Timestamp], new GsonUTCDateAdapter).create

  def toJson(src: Any): String = {
    gson.toJson(src)
  }

  def fromJson[T: ClassTag](json: String): T = {
    try {
      gson.fromJson[T](json, classTag[T].runtimeClass)
    } catch {
      case e: Throwable =>
        LogManager.getRootLogger.warn("Can't parse to json: " + classTag[T].toString() + "\n" + e)
        throw e
    }
  }
}
