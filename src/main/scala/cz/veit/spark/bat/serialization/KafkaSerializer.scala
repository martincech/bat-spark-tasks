package cz.veit.spark.bat.serialization

import java.util

import org.apache.kafka.common.serialization.{Serializer, StringSerializer}

class KafkaSerializer[A] extends Serializer[A] {
  private val stringSerializer = new StringSerializer

  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit =
    stringSerializer.configure(configs, isKey)

  override def serialize(topic: String, data: A): Array[Byte] = {

    stringSerializer.serialize(topic, JsonSerializer.toJson(data))
  }
  override def close(): Unit =
    stringSerializer.close()

}

