package cz.veit.spark.bat.dataobjects

import java.sql.Timestamp

case class DailyWeightAggregation(Uid: String,
                                  TimeStamp: Timestamp,
                                  Sex: String,

                                  Count: Int,
                                  Day: Int,
                                  Average: Double,
                                  Gain: Double,
                                  Sigma: Double,
                                  Cv: Double,
                                  Uniformity: Double
                          )
