package cz.veit.spark.bat.dataobjects

import java.sql.Timestamp

case class Group(Group: String,
                 From: Timestamp = null,
                 To: Timestamp = null)
