package cz.veit.spark.bat.dataobjects

import java.sql.Timestamp

case class AggregationResult(
                              id: String,
                              database: String,
                              intervalStart: Timestamp,
                              intervalEnd: Timestamp,
                              sex: String,
                              topic: String,

                              count: Double,
                              average: Double,
                              min: Double,
                              max: Double,
                              sigma: Double,
                              cv: Double,
                              gain: Double,
                              uniformity: Double
                            )
