package cz.veit.spark.bat.dataobjects

import java.sql.Timestamp

case class TransformedRawData(id: String,
                              topic: String,
                              value: Double,
                              timestamp: Timestamp,
                              sex: String = null,
                              difference: String = null)


