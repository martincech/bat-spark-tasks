package cz.veit.spark.bat.dataobjects.topicnames

import scala.concurrent.duration.FiniteDuration


object NoGainTopic extends Enumeration {
  private val noGain = "_NoGain"

  def strip(topic: String): String = {
    topic.stripPrefix(noGain)
  }
  def apply(name: String, duration: FiniteDuration): String = {
    (noGain + name + "_" + duration.toString).filterNot(x => x.isWhitespace)
  }
}