package cz.veit.spark.bat.dataobjects.topicnames

object RawDataTopic extends Enumeration {
  type RawDataTopic = Value
  val Weight: Value = Value("BirdWeight")
  val Temperature: Value = Value("Temperature")
  val Humidity: Value = Value("Humidity")
  val Co2: Value = Value("Co2")
  val Unknown: Value = Value("")

  def AllTopics: Seq[String] =
    RawDataTopic.Temperature.toString ::
      RawDataTopic.Humidity.toString ::
      RawDataTopic.Co2.toString ::
      RawDataTopic.Weight.toString :: Nil

  def AllTopicsExceptWeight: Seq[String] =
    RawDataTopic.Temperature.toString ::
      RawDataTopic.Humidity.toString ::
      RawDataTopic.Co2.toString :: Nil

  def apply(topic: RawDataTopic) : String = {
    topic.toString
  }
  def apply(name: String): RawDataTopic =
    values.find(_.toString.toLowerCase == name.toLowerCase()).getOrElse(Unknown)
}