package cz.veit.spark.bat.dataobjects.topicnames

object DailyTopic extends Enumeration {
  type DailyWeightTopic = Value
  val DailyWeight = Value("DailyWeight")

  private val daily = "Daily"

  def strip(topic: String): String = {
    topic.stripPrefix(daily)
  }
  def apply(name: String): String = {
    (daily + name).filterNot(x => x.isWhitespace)
  }
}