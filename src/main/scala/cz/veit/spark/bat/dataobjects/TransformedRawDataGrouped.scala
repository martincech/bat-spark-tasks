package cz.veit.spark.bat.dataobjects

import java.sql.Timestamp

case class TransformedRawDataGrouped(database:String,
                                     groupId: String,
                                     value: Double,
                                     timestamp: Timestamp,
                                     sex: String = null,
                                     difference: String = null)
