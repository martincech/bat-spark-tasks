package cz.veit.spark.bat.dataobjects

case class GroupEvent(Uid: String, Database: String, Groups: Array[Group])