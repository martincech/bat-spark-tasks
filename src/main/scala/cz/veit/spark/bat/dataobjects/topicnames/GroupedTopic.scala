package cz.veit.spark.bat.dataobjects.topicnames

object GroupedTopic extends Enumeration {

  private val grouped = "_Grouped_"
  type GroupTopic = Value
  val Group = Value("Groups")

  def strip(name: String): String = {
    name.stripPrefix(grouped)
  }
  def apply(name : String): String = {
    (grouped + name).filterNot(x => x.isWhitespace)
  }

}