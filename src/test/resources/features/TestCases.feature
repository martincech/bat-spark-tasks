@ApacheSpark
Feature: Transformation and aggregations tests

  Background: Influx and Kafka runs
    Given Influx is on "localhost" port 8086
    Given Kafka is on "localhost" port 9092

  Scenario Outline: New format transformation for the sensor <Sensor>
    Given Influx databases "<Database>" with retention policies "raw" set to "365 days" exists
    And measurements "<Measurement>" do not exist in database "<Database>"
    When following data are send to kafka server to the topic "<Topic>":
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T00:00:00Z","Value":0.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T00:30:00Z","Value":0.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T01:00:00Z","Value":1.0,"SensorUid":"<Sensor>"}} |
    Then after "10 seconds" Influx database "<Database>" should contain the measurement "<Measurement>" with following records:
      | <Today>T00:00:00Z, <Sensor>-T-Test, 0   |
      | <Today>T00:30:00Z, <Sensor>-T-Test, 0.5 |
      | <Today>T01:00:00Z, <Sensor>-T-Test, 1   |
    And the count of record in the measurement "<Measurement>" in the Influx is 3

    Examples:
      | Sensor | Topic       | Measurement         | Database |
      | TE1    | Temperature | "raw"."temperature" | samples  |
      | HU1    | Humidity    | "raw"."humidity"    | samples  |
      | CO1    | Co2         | "raw"."co2"         | samples  |
      | BW1    | BirdWeight  | "raw"."weight"      | samples  |

  Scenario Outline: Old format transformation for the sensor <Sensor>
    Given Influx databases "<Database>" with retention policies "raw" set to "365 days" exists
    And measurements "<Measurement>" do not exist in database "<Database>"
    When following data are send to kafka server to the topic "<Topic>":
      | {"TerminalSn":1,"SensorPackSn":<Sensor>,"Data":{"TimeStamp":"<Today>T00:00:00Z","Value":0.0}} |
      | {"TerminalSn":1,"SensorPackSn":<Sensor>,"Data":{"TimeStamp":"<Today>T00:30:00Z","Value":0.5}} |
      | {"TerminalSn":1,"SensorPackSn":<Sensor>,"Data":{"TimeStamp":"<Today>T01:00:00Z","Value":1.0}} |
    Then after "10 seconds" Influx database "<Database>" should contain the measurement "<Measurement>" with following records:
      | <Today>T00:00:00Z, <Sensor>-T-1, 0   |
      | <Today>T00:30:00Z, <Sensor>-T-1, 0.5 |
      | <Today>T01:00:00Z, <Sensor>-T-1, 1   |
    And the count of record in the measurement "<Measurement>" in the Influx is 3

    Examples:
      | Sensor | Topic       | Measurement         | Database |
      | 102    | Temperature | "raw"."temperature" | samples  |
      | 112    | Humidity    | "raw"."humidity"    | samples  |
      | 122    | Co2         | "raw"."co2"         | samples  |
      | 132    | BirdWeight  | "raw"."weight"      | samples  |

  Scenario Outline: Daily and half hour aggregations are computed correctly for raw data from <Sensor> for 2 days
    Given Influx databases "samples,<Database>" with retention policies "raw, 30_m,1440_m" set to "365 days" exists
    And measurements "<Measurement_30m>, <Measurement_1d>" do not exist in database "<Database>"

    When following data are send to kafka server to the topic "Groups":
      | {"Uid":"<DeviceName>","Database":"<Database>","Groups":[ {"Group":"<DeviceName>","From":"<Today>T00:00:00Z","To":"<Tomorrow>T23:59:59Z"}]} |
    And following data are send to kafka server to the topic "<Topic>":
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T00:00:00Z","Value":10.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T00:30:00Z","Value":10.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T01:00:00Z","Value":11.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T01:30:00Z","Value":11.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T02:00:00Z","Value":12.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T02:30:00Z","Value":12.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T03:00:00Z","Value":13.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T03:30:00Z","Value":13.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T04:00:00Z","Value":14.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T04:30:00Z","Value":14.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T05:00:00Z","Value":15.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T05:30:00Z","Value":15.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T06:00:00Z","Value":16.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T06:30:00Z","Value":16.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T07:00:00Z","Value":17.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T07:30:00Z","Value":17.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T08:00:00Z","Value":18.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T08:30:00Z","Value":18.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T09:00:00Z","Value":19.0,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T09:30:00Z","Value":19.5,"SensorUid":"<Sensor>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T10:00:00Z","Value":110.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T10:30:00Z","Value":110.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T11:00:00Z","Value":111.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T11:30:00Z","Value":111.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T12:00:00Z","Value":112.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T12:30:00Z","Value":112.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T13:00:00Z","Value":113.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T13:30:00Z","Value":113.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T14:00:00Z","Value":114.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T14:30:00Z","Value":114.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T15:00:00Z","Value":115.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T15:30:00Z","Value":115.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T16:00:00Z","Value":116.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T16:30:00Z","Value":116.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T17:00:00Z","Value":117.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T17:30:00Z","Value":117.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T18:00:00Z","Value":118.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T18:30:00Z","Value":118.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T19:00:00Z","Value":119.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T19:30:00Z","Value":119.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T20:00:00Z","Value":120.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T20:30:00Z","Value":120.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T21:00:00Z","Value":121.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T21:30:00Z","Value":121.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T22:00:00Z","Value":122.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T22:30:00Z","Value":122.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T23:00:00Z","Value":123.0,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T23:30:00Z","Value":123.5,"SensorUid":"<Sensor>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T00:00:00Z","Value":20.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T00:30:00Z","Value":20.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T01:00:00Z","Value":21.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T01:30:00Z","Value":21.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T02:00:00Z","Value":22.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T02:30:00Z","Value":22.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T03:00:00Z","Value":23.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T03:30:00Z","Value":23.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T04:00:00Z","Value":24.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T04:30:00Z","Value":24.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T05:00:00Z","Value":25.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T05:30:00Z","Value":25.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T06:00:00Z","Value":26.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T06:30:00Z","Value":26.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T07:00:00Z","Value":27.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T07:30:00Z","Value":27.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T08:00:00Z","Value":28.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T08:30:00Z","Value":28.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T09:00:00Z","Value":29.0,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T09:30:00Z","Value":29.5,"SensorUid":"<Sensor>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T10:00:00Z","Value":210.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T10:30:00Z","Value":210.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T11:00:00Z","Value":211.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T11:30:00Z","Value":211.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T12:00:00Z","Value":212.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T12:30:00Z","Value":212.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T13:00:00Z","Value":213.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T13:30:00Z","Value":213.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T14:00:00Z","Value":214.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T14:30:00Z","Value":214.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T15:00:00Z","Value":215.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T15:30:00Z","Value":215.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T16:00:00Z","Value":216.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T16:30:00Z","Value":216.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T17:00:00Z","Value":217.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T17:30:00Z","Value":217.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T18:00:00Z","Value":218.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T18:30:00Z","Value":218.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T19:00:00Z","Value":219.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T19:30:00Z","Value":219.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T20:00:00Z","Value":220.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T20:30:00Z","Value":220.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T21:00:00Z","Value":221.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T21:30:00Z","Value":221.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T22:00:00Z","Value":222.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T22:30:00Z","Value":222.5,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T23:00:00Z","Value":223.0,"SensorUid":"<Sensor>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T23:30:00Z","Value":223.5,"SensorUid":"<Sensor>"}} |

    Then after "5 minutes" Influx database "<Database>" should contain the measurement "<Measurement_30m>" with following records:
      | <Today>T00:00:00Z, 10, 1, 0, 0, 10, 10, 0, 100                 |
      | <Today>T00:30:00Z, 10.5, 1, 0, 0.5, 10.5, 10.5, 0, 100         |
      | <Today>T01:00:00Z, 11, 1, 0, 0.5, 11, 11, 0, 100               |
      | <Today>T01:30:00Z, 11.5, 1, 0, 0.5, 11.5, 11.5, 0, 100         |
      | <Today>T02:00:00Z, 12, 1, 0, 0.5, 12, 12, 0, 100               |
      | <Today>T02:30:00Z, 12.5, 1, 0, 0.5, 12.5, 12.5, 0, 100         |
      | <Today>T03:00:00Z, 13, 1, 0, 0.5, 13, 13, 0, 100               |
      | <Today>T03:30:00Z, 13.5, 1, 0, 0.5, 13.5, 13.5, 0, 100         |
      | <Today>T04:00:00Z, 14, 1, 0, 0.5, 14, 14, 0, 100               |
      | <Today>T04:30:00Z, 14.5, 1, 0, 0.5, 14.5, 14.5, 0, 100         |
      | <Today>T05:00:00Z, 15, 1, 0, 0.5, 15, 15, 0, 100               |
      | <Today>T05:30:00Z, 15.5, 1, 0, 0.5, 15.5, 15.5, 0, 100         |
      | <Today>T06:00:00Z, 16, 1, 0, 0.5, 16, 16, 0, 100               |
      | <Today>T06:30:00Z, 16.5, 1, 0, 0.5, 16.5, 16.5, 0, 100         |
      | <Today>T07:00:00Z, 17, 1, 0, 0.5, 17, 17, 0, 100               |
      | <Today>T07:30:00Z, 17.5, 1, 0, 0.5, 17.5, 17.5, 0, 100         |
      | <Today>T08:00:00Z, 18, 1, 0, 0.5, 18, 18, 0, 100               |
      | <Today>T08:30:00Z, 18.5, 1, 0, 0.5, 18.5, 18.5, 0, 100         |
      | <Today>T09:00:00Z, 19, 1, 0, 0.5, 19, 19, 0, 100               |
      | <Today>T09:30:00Z, 19.5, 1, 0, 0.5, 19.5, 19.5, 0, 100         |
      | <Today>T10:00:00Z, 110, 1, 0, 90.5, 110, 110, 0, 100           |
      | <Today>T10:30:00Z, 110.5, 1, 0, 0.5, 110.5, 110.5, 0, 100      |
      | <Today>T11:00:00Z, 111, 1, 0, 0.5, 111, 111, 0, 100            |
      | <Today>T11:30:00Z, 111.5, 1, 0, 0.5, 111.5, 111.5, 0, 100      |
      | <Today>T12:00:00Z, 112, 1, 0, 0.5, 112, 112, 0, 100            |
      | <Today>T12:30:00Z, 112.5, 1, 0, 0.5, 112.5, 112.5, 0, 100      |
      | <Today>T13:00:00Z, 113, 1, 0, 0.5, 113, 113, 0, 100            |
      | <Today>T13:30:00Z, 113.5, 1, 0, 0.5, 113.5, 113.5, 0, 100      |
      | <Today>T14:00:00Z, 114, 1, 0, 0.5, 114, 114, 0, 100            |
      | <Today>T14:30:00Z, 114.5, 1, 0, 0.5, 114.5, 114.5, 0, 100      |
      | <Today>T15:00:00Z, 115, 1, 0, 0.5, 115, 115, 0, 100            |
      | <Today>T15:30:00Z, 115.5, 1, 0, 0.5, 115.5, 115.5, 0, 100      |
      | <Today>T16:00:00Z, 116, 1, 0, 0.5, 116, 116, 0, 100            |
      | <Today>T16:30:00Z, 116.5, 1, 0, 0.5, 116.5, 116.5, 0, 100      |
      | <Today>T17:00:00Z, 117, 1, 0, 0.5, 117, 117, 0, 100            |
      | <Today>T17:30:00Z, 117.5, 1, 0, 0.5, 117.5, 117.5, 0, 100      |
      | <Today>T18:00:00Z, 118, 1, 0, 0.5, 118, 118, 0, 100            |
      | <Today>T18:30:00Z, 118.5, 1, 0, 0.5, 118.5, 118.5, 0, 100      |
      | <Today>T19:00:00Z, 119, 1, 0, 0.5, 119, 119, 0, 100            |
      | <Today>T19:30:00Z, 119.5, 1, 0, 0.5, 119.5, 119.5, 0, 100      |
      | <Today>T20:00:00Z, 120, 1, 0, 0.5, 120, 120, 0, 100            |
      | <Today>T20:30:00Z, 120.5, 1, 0, 0.5, 120.5, 120.5, 0, 100      |
      | <Today>T21:00:00Z, 121, 1, 0, 0.5, 121, 121, 0, 100            |
      | <Today>T21:30:00Z, 121.5, 1, 0, 0.5, 121.5, 121.5, 0, 100      |
      | <Today>T22:00:00Z, 122, 1, 0, 0.5, 122, 122, 0, 100            |
      | <Today>T22:30:00Z, 122.5, 1, 0, 0.5, 122.5, 122.5, 0, 100      |
      | <Today>T23:00:00Z, 123, 1, 0, 0.5, 123, 123, 0, 100            |
      | <Today>T23:30:00Z, 123.5, 1, 0, 0.5, 123.5, 123.5, 0, 100      |
      | <Tomorrow>T00:00:00Z, 20, 1, 0, -103.5, 20, 20, 0, 100         |
      | <Tomorrow>T00:30:00Z, 20.5, 1, 0, 0.5, 20.5, 20.5, 0, 100      |
      | <Tomorrow>T01:00:00Z, 21, 1, 0, 0.5, 21, 21, 0, 100            |
      | <Tomorrow>T01:30:00Z, 21.5, 1, 0, 0.5, 21.5, 21.5, 0, 100      |
      | <Tomorrow>T02:00:00Z, 22, 1, 0, 0.5, 22, 22, 0, 100            |
      | <Tomorrow>T02:30:00Z, 22.5, 1, 0, 0.5, 22.5, 22.5, 0, 100      |
      | <Tomorrow>T03:00:00Z, 23, 1, 0, 0.5, 23, 23, 0, 100            |
      | <Tomorrow>T03:30:00Z, 23.5, 1, 0, 0.5, 23.5, 23.5, 0, 100      |
      | <Tomorrow>T04:00:00Z, 24, 1, 0, 0.5, 24, 24, 0, 100            |
      | <Tomorrow>T04:30:00Z, 24.5, 1, 0, 0.5, 24.5, 24.5, 0, 100      |
      | <Tomorrow>T05:00:00Z, 25, 1, 0, 0.5, 25, 25, 0, 100            |
      | <Tomorrow>T05:30:00Z, 25.5, 1, 0, 0.5, 25.5, 25.5, 0, 100      |
      | <Tomorrow>T06:00:00Z, 26, 1, 0, 0.5, 26, 26, 0, 100            |
      | <Tomorrow>T06:30:00Z, 26.5, 1, 0, 0.5, 26.5, 26.5, 0, 100      |
      | <Tomorrow>T07:00:00Z, 27, 1, 0, 0.5, 27, 27, 0, 100            |
      | <Tomorrow>T07:30:00Z, 27.5, 1, 0, 0.5, 27.5, 27.5, 0, 100      |
      | <Tomorrow>T08:00:00Z, 28, 1, 0, 0.5, 28, 28, 0, 100            |
      | <Tomorrow>T08:30:00Z, 28.5, 1, 0, 0.5, 28.5, 28.5, 0, 100      |
      | <Tomorrow>T09:00:00Z, 29, 1, 0, 0.5, 29, 29, 0, 100            |
      | <Tomorrow>T09:30:00Z, 29.5, 1, 0, 0.5, 29.5, 29.5, 0, 100      |
      | <Tomorrow>T10:00:00Z, 210, 1, 0, 180.5, 210, 210, 0, 100       |
      | <Tomorrow>T10:30:00Z, 210.5, 1, 0, 0.5, 210.5, 210.5, 0, 100   |
      | <Tomorrow>T11:00:00Z, 211, 1, 0, 0.5, 211, 211, 0, 100         |
      | <Tomorrow>T11:30:00Z, 211.5, 1, 0, 0.5, 211.5, 211.5, 0, 100   |
      | <Tomorrow>T12:00:00Z, 212, 1, 0, 0.5, 212, 212, 0, 100         |
      | <Tomorrow>T12:30:00Z, 212.5, 1, 0, 0.5, 212.5, 212.5, 0, 100   |
      | <Tomorrow>T13:00:00Z, 213, 1, 0, 0.5, 213, 213, 0, 100         |
      | <Tomorrow>T13:30:00Z, 213.5, 1, 0, 0.5, 213.5, 213.5, 0, 100   |
      | <Tomorrow>T14:00:00Z, 214, 1, 0, 0.5, 214, 214, 0, 100         |
      | <Tomorrow>T14:30:00Z, 214.5, 1, 0, 0.5, 214.5, 214.5, 0, 100   |
      | <Tomorrow>T15:00:00Z, 215, 1, 0, 0.5, 215, 215, 0, 100         |
      | <Tomorrow>T15:30:00Z, 215.5, 1, 0, 0.5, 215.5, 215.5, 0, 100   |
      | <Tomorrow>T16:00:00Z, 216, 1, 0, 0.5, 216, 216, 0, 100         |
      | <Tomorrow>T16:30:00Z, 216.5, 1, 0, 0.5, 216.5, 216.5, 0, 100   |
      | <Tomorrow>T17:00:00Z, 217, 1, 0, 0.5, 217, 217, 0, 100         |
      | <Tomorrow>T17:30:00Z, 217.5, 1, 0, 0.5, 217.5, 217.5, 0, 100   |
      | <Tomorrow>T18:00:00Z, 218, 1, 0, 0.5, 218, 218, 0, 100         |
      | <Tomorrow>T18:30:00Z, 218.5, 1, 0, 0.5, 218.5, 218.5, 0, 100   |
      | <Tomorrow>T19:00:00Z, 219, 1, 0, 0.5, 219, 219, 0, 100         |
      | <Tomorrow>T19:30:00Z, 219.5, 1, 0, 0.5, 219.5, 219.5, 0, 100   |
      | <Tomorrow>T20:00:00Z, 220, 1, 0, 0.5, 220, 220, 0, 100         |
      | <Tomorrow>T20:30:00Z, 220.5, 1, 0, 0.5, 220.5, 220.5, 0, 100   |
      | <Tomorrow>T21:00:00Z, 221, 1, 0, 0.5, 221, 221, 0, 100         |
      | <Tomorrow>T21:30:00Z, 221.5, 1, 0, 0.5, 221.5, 221.5, 0, 100   |
      | <Tomorrow>T22:00:00Z, 222, 1, 0, 0.5, 222, 222, 0, 100         |
      | <Tomorrow>T22:30:00Z, 222.5, 1, 0, 0.5, 222.5, 222.5, 0, 100   |
      | <Tomorrow>T23:00:00Z, 223, 1, 0, 0.5, 223, 223, 0, 100         |
      | <Tomorrow>T23:30:00Z, 223.5, 1, 0, 0.5, 223.5, 223.5, 0, 100   |
      | <AfterTomorrow>T00:00:00Z, 223.5, 1, 0, 0, 223.5, 223.5, 0, 100 |

    And after "1 second" Influx database "<Database>" should contain the measurement "<Measurement_1d>" with following records:
      | <Today>T00:00:00Z, 74.25, 48, 68.61828636099322, 0, 123.5, 10, 50.949077623037475, 0         |
      | <Tomorrow>T00:00:00Z, 136.75, 48, 70.00236043931808, 62.5, 223.5, 20, 95.72822790076746, 0   |
      | <AfterTomorrow>T00:00:00Z, 136.75, 48, 70.00236043931808, 0, 223.5, 20, 95.72822790076746, 0 |

    Examples:
      | Sensor | Topic       | Database | DeviceName | Measurement_30m                     | Measurement_1d                          |
      | TE3    | Temperature | db1      | TE3-T-Test | "30_m"."TE3-T-Test_temperature_30m" | "1440_m"."TE3-T-Test_temperature_1440m" |
      | HU3    | Humidity    | db1      | HU3-T-Test | "30_m"."HU3-T-Test_humidity_30m"    | "1440_m"."HU3-T-Test_humidity_1440m"    |
      | CO3    | Co2         | db1      | CO3-T-Test | "30_m"."CO3-T-Test_co2_30m"         | "1440_m"."CO3-T-Test_co2_1440m"         |
      | BW3    | BirdWeight  | db1      | BW3-T-Test | "30_m"."BW3-T-Test_weight_30m"      | "1440_m"."BW3-T-Test_weight_1440m"      |

  Scenario Outline: Combination of raw data aggregation together with SMS aggregated data gives correct results for each sex
    Given Influx databases "samples,<Database>" with retention policies "raw, 30_m,1440_m" set to "365 days" exists
    And measurements "<Measurement_1d>" do not exist in database "<Database>"

    When following data are send to kafka server to the topic "Groups":
      | {"Uid":"<DeviceName>","Database":"<Database>","Groups":[ {"Group":"<DeviceName>","From":"<Today>T00:00:00Z","To":"<Tomorrow>T23:59:59Z"},{"Group":"<GroupName>","From":"<Today>T00:00:00Z","To":"<Tomorrow>T23:59:59Z"}]}                     |
      | {"Uid":"<AggregatedDeviceName>","Database":"<Database>","Groups":[ {"Group":"<AggregatedDeviceName>","From":"<Today>T00:00:00Z","To":"<Tomorrow>T23:59:59Z"},{"Group":"<GroupName>","From":"<Today>T00:00:00Z","To":"<Tomorrow>T23:59:59Z"}]} |
    And following data are send to kafka server to the topic "<Topic>":
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T00:00:00Z","Value":10.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T00:30:00Z","Value":10.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T01:00:00Z","Value":11.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T01:30:00Z","Value":11.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T02:00:00Z","Value":12.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T02:30:00Z","Value":12.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T03:00:00Z","Value":13.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T03:30:00Z","Value":13.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T04:00:00Z","Value":14.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T04:30:00Z","Value":14.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T05:00:00Z","Value":15.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T05:30:00Z","Value":15.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T06:00:00Z","Value":16.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T06:30:00Z","Value":16.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T07:00:00Z","Value":17.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T07:30:00Z","Value":17.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T08:00:00Z","Value":18.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T08:30:00Z","Value":18.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T09:00:00Z","Value":19.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T09:30:00Z","Value":19.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}     |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T10:00:00Z","Value":110.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T10:30:00Z","Value":110.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T11:00:00Z","Value":111.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T11:30:00Z","Value":111.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T12:00:00Z","Value":112.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T12:30:00Z","Value":112.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T13:00:00Z","Value":113.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T13:30:00Z","Value":113.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T14:00:00Z","Value":114.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T14:30:00Z","Value":114.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T15:00:00Z","Value":115.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T15:30:00Z","Value":115.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T16:00:00Z","Value":116.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T16:30:00Z","Value":116.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T17:00:00Z","Value":117.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T17:30:00Z","Value":117.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T18:00:00Z","Value":118.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T18:30:00Z","Value":118.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T19:00:00Z","Value":119.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T19:30:00Z","Value":119.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T20:00:00Z","Value":120.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T20:30:00Z","Value":120.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T21:00:00Z","Value":121.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T21:30:00Z","Value":121.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T22:00:00Z","Value":122.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T22:30:00Z","Value":122.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T23:00:00Z","Value":123.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Today>T23:30:00Z","Value":123.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}    |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T00:00:00Z","Value":20.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T00:30:00Z","Value":20.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T01:00:00Z","Value":21.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T01:30:00Z","Value":21.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T02:00:00Z","Value":22.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T02:30:00Z","Value":22.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T03:00:00Z","Value":23.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T03:30:00Z","Value":23.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T04:00:00Z","Value":24.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T04:30:00Z","Value":24.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T05:00:00Z","Value":25.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T05:30:00Z","Value":25.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T06:00:00Z","Value":26.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T06:30:00Z","Value":26.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T07:00:00Z","Value":27.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T07:30:00Z","Value":27.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T08:00:00Z","Value":28.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T08:30:00Z","Value":28.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T09:00:00Z","Value":29.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T09:30:00Z","Value":29.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}}  |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T10:00:00Z","Value":210.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T10:30:00Z","Value":210.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T11:00:00Z","Value":211.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T11:30:00Z","Value":211.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T12:00:00Z","Value":212.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T12:30:00Z","Value":212.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T13:00:00Z","Value":213.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T13:30:00Z","Value":213.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T14:00:00Z","Value":214.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T14:30:00Z","Value":214.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T15:00:00Z","Value":215.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T15:30:00Z","Value":215.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T16:00:00Z","Value":216.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T16:30:00Z","Value":216.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T17:00:00Z","Value":217.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T17:30:00Z","Value":217.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T18:00:00Z","Value":218.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T18:30:00Z","Value":218.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T19:00:00Z","Value":219.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T19:30:00Z","Value":219.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T20:00:00Z","Value":220.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T20:30:00Z","Value":220.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T21:00:00Z","Value":221.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T21:30:00Z","Value":221.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T22:00:00Z","Value":222.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T22:30:00Z","Value":222.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T23:00:00Z","Value":223.0,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |
      | {"TerminalSn":"Test","Data":{"TimeStamp":"<Tomorrow>T23:30:00Z","Value":223.5,"SensorUid":"<Sensor>","Sex":"<Sex>"}} |

    And following data are send to kafka server to the topic "DailyWeight":
      | {"Uid":"<AggregatedDeviceName>","TimeStamp":"<Today>T00:00:00Z","Count":25,"Day":1,"Average":83.0,"Gain":0.0,"Sigma":65.3748,"Cv":78.76481927710840,"Uniformity":55.0,"Sex":"<Sex>"} |
      | {"Uid":"<AggregatedDeviceName>","TimeStamp":"<Tomorrow>T00:00:00Z","Count":150,"Day":2,"Average":125.0,"Gain":42.0,"Sigma":67,65789,"Cv":54,126312,"Uniformity":84,25,"Sex":"<Sex>"} |

    Then after "5 minutes" Influx database "<Database>" should contain the measurement "<Measurement_1d>" with following records:
      | <Today>T00:00:00Z, <Sex>, 77.24657534246570, 73, 72.09312640075870, 0, 123.5, 10, 55.88939350555890, 18.83561643835620 |
      | <Tomorrow>T00:00:00Z, <Sex>, 127.8484848, 198, 57.97505102, 32.32323232, 223.5, 20, 74.4628204, 63.82575758            |
      | <AfterTomorrow>T00:00:00Z, <Sex>, 127.8484848, 198, 57.97505102, 0, 223.5, 20, 74.4628204, 63.82575758                 |

    Examples:
      | Sensor | Topic      | Database | DeviceName | AggregatedDeviceName | GroupName | Measurement_1d                 | Sex       |
      | BW4    | BirdWeight | db1      | BW4-T-Test | Agg-T-Test           | Group1    | "1440_m"."Group1_weight_1440m" | male      |
      | BW4    | BirdWeight | db1      | BW4-T-Test | Agg-T-Test           | Group1    | "1440_m"."Group1_weight_1440m" | female    |
      | BW4    | BirdWeight | db1      | BW4-T-Test | Agg-T-Test           | Group1    | "1440_m"."Group1_weight_1440m" | undefined |

  Scenario Outline: Daily and half hour aggregations are computed correctly for generated raw data for the sensor <Sensor>
    Given Influx databases "samples,<Database>" with retention policies "raw, 30_m,1440_m" set to "365 days" exists
    And measurements "<Measurement_30m>, <Measurement_1d>" do not exist in database "<Database>"

    Given we've generated raw data to the file "datafile.<Sensor>"
    And we've the expected results to the file "datafile.<Sensor>" for period "30m" in the file "expected30m.<Sensor>"
    And we've the expected results to the file "datafile.<Sensor>" for period "1d" in the file "expected1d.<Sensor>"

    When following data are send to kafka server to the topic "Groups":
      | {"Uid":"<DeviceName>","Database":"<Database>","Groups":[ {"Group":"<DeviceName>","From":"<Today>T00:00:00Z","To":"<Tomorrow>T23:59:59Z"}]} |
    And the data from the file "datafile.<Sensor>" are send to kafka server to the topic "<Topic>"

    Then after "5 minutes" Influx database "<Database>" should contain the measurement "<Measurement_30m>" with the records from the file "expected30m.<Topic>":
    And after "1 second" Influx database "<Database>" should contain the measurement "<Measurement_1d>" with the records from the file "expected1d.<Topic>":

    Examples:
      | Sensor | Topic       | Database | DeviceName | Measurement_30m                     | Measurement_1d                          |
      | TE4    | Temperature | db1      | TE4-T-Test | "30_m"."TE4-T-Test_temperature_30m" | "1440_m"."TE4-T-Test_temperature_1440m" |
      | HU4    | Humidity    | db1      | HU4-T-Test | "30_m"."HU4-T-Test_humidity_30m"    | "1440_m"."HU4-T-Test_humidity_1440m"    |
      | CO4    | Co2         | db1      | CO4-T-Test | "30_m"."CO4-T-Test_co2_30m"         | "1440_m"."CO4-T-Test_co2_1440m"         |
      | BW5    | BirdWeight  | db1      | BW5-T-Test | "30_m"."BW5-T-Test_weight_30m"      | "1440_m"."BW5-T-Test_weight_1440m"      |
