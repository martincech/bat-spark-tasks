package steps

import java.text.SimpleDateFormat
import java.util
import java.util.{Calendar, Date}
import com.paulgoldbaum.influxdbclient.{Database, InfluxDB}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig}
import org.junit.Assert.{assertFalse, assertTrue}
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

object Utilities {


  def replaceTimes(str: String): String = {
    val replacements = Map[String, Date](
      "<Today>" -> Calendar.getInstance().getTime,
      "<Tomorrow>" -> {
        val calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        calendar.getTime
      },
      "<AfterTomorrow>" -> {
        val calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 2);
        calendar.getTime
      }
    )

    def replaceStringByDate(str: String, what: String): String = {
      val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
      if (replacements.contains(what)) {
        str.replace(what, dateFormat.format(replacements(what)))
      } else {
        str
      }
    }

    replacements.foldLeft(str)((s, repl) => replaceStringByDate(s, repl._1))
  }
  def toSeq(str: String): Seq[String] = {
    str.filterNot(x => x.isWhitespace).split(',').toSeq
  }

  def givenInfluxExists(host: String, port: Int): InfluxDB = {
    InfluxDB.connect(host, port)
  }

  def givenKafkaExists(broker: String): KafkaProducer[String, String] = {
    val props = new util.HashMap[String, Object]()
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, broker)
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
      "org.apache.kafka.common.serialization.StringSerializer")
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
      "org.apache.kafka.common.serialization.StringSerializer")
    new KafkaProducer[String, String](props)
  }

  def givenInfluxDbExists(influxDb: InfluxDB, databaseNames: String, policies: String, policiesDuration: String): Map[String, Database] = {
    def rpExists(database: Database, rp: String): Boolean = {
      val result = Await.result(database.showRetentionPolicies(), Duration.Inf)
      result.series.head.records.exists(r => r("name") == rp)
    }

    def dbExists(database: Database): Boolean = {
      Await.result(database.exists(), Duration.Inf)
    }

    val seq = toSeq(databaseNames).flatMap(dtb => {
      toSeq(policies).flatMap(rp => {
        toSeq(policiesDuration).map(policyDuration => {
          val database = influxDb.selectDatabase(dtb)
          //create DB
          if (!dbExists(database)) {
            Await.result(database.create(), Duration.Inf)
          }
          assertTrue(dbExists(database))
          //create RP
          val duration = Duration.apply(policyDuration).toHours + "h"
          if (!rpExists(database, rp)) {
            Await.ready(database.createRetentionPolicy(rp, duration, 1, default = true), Duration.Inf)
          }
          assertTrue(rpExists(database, rp))
          dtb -> database
        })
      })
    })
    Map[String, Database](seq: _*)
  }

  def givenMeasurementsDoNotExistsInDatabase(measurements: String, database: Database): Unit = {
    def measurementExists(measurement: String): Boolean = {
      val measurements = Await.result(database.query("show measurements"), Duration.Inf)

      val mesName = measurement.filterNot(s => s == '"')

      measurements.series.nonEmpty &&
        measurements.series.head.records.nonEmpty &&
        measurements.series.head.records.map(record => {
          val name = record("name")
          if (name == mesName) true else false
        }).foldLeft(false)((a, b) => a || b)
    }

    toSeq(measurements).map(m=>m.split('.').last ).foreach(measurement => {
      if (measurementExists(measurement)) {
        Await.result(database.query("drop measurement " + measurement), Duration.Inf)
      }
      assertFalse(measurementExists(measurement))
    })
  }
}
