package steps

import com.paulgoldbaum.influxdbclient._
import cucumber.api.scala.{EN, ScalaDsl}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.junit.Assert._
import scala.collection.JavaConversions._
import scala.concurrent._
import scala.concurrent.duration._
import scala.io.Source

class Features extends ScalaDsl with EN {
  var producer: KafkaProducer[String, String] = _
  var influxDb: InfluxDB = _
  var databases: Map[String, Database] = Map[String, Database]()

  def sendToTopic(topic: String, lines: java.util.List[String]): Unit = {
    for (line <- lines) {
      producer.send(new ProducerRecord(
        topic, Utilities.replaceTimes(line)
      ))
    }
    producer.flush()
  }

  Given("""^Influx is on "(.*)" port (\d+)$""") {
    (host: String, port: Int) =>
      influxDb = Utilities.givenInfluxExists(host, port)
  }

  Given("""^Kafka is on "(.*)" port (\d+)$""") {
    (host: String, port: Int) =>
      producer = Utilities.givenKafkaExists(host + ":" + port)

  }

  Given("""^Influx databases "(.*)" with retention policies "(.*)" set to "(.*)" exists$""") {
    (databaseNames: String, policies: String, policiesDuration: String) =>
      databases = Utilities.givenInfluxDbExists(influxDb, databaseNames, policies, policiesDuration)
  }

  Given("""^measurements "(.*)" do not exist in database "(.*)"$""") {
    (measurements: String, dbName: String) =>
      Utilities.givenMeasurementsDoNotExistsInDatabase(measurements, databases(dbName))
  }

  Given("""^we've generated raw data to the file "(.*)"$""") {
    (fileName: String) =>
    //TODO
  }

  Given("""^we've the expected results to the file "(.*)" for period "(.*)" in the file "(.*)"$""") {
    (generatedDataFile: String, period: String, expectedDataFile: String) =>
    //TODO
  }


  When("""^following data are send to kafka server to the topic "(.*)":$""") {
    (topic: String, lines: java.util.List[String]) =>
      sendToTopic(topic, lines)
  }

  When("""^the data from the file "(.*)" are send to kafka server to the topic "(.*)"$""") {
    (file: String, topic: String) =>
      sendToTopic(topic, Source.fromFile(file, "UTF-8").getLines.toList)
  }

  def compareExpectedResult(database: Database, measurement: String, expectedResult: Array[String]): Unit = {
    val result = Await.result(database.query("select * from " + measurement), Duration.Inf)
    assertTrue("No series for measurement " + measurement + " in the database " + database.databaseName, result.series.nonEmpty)
    assertTrue("No results for measurement " + measurement + " in the database " + database.databaseName, result.series.head.records.nonEmpty)

    result.series.head.records.zipWithIndex.foreach(record => {
      val row = record._1
      val i = record._2
      val rowAsString = row.allValues.mkString(", ")
      assertEquals(expectedResult(i), rowAsString)
    })
  }

  Then("""^after "(.*)" Influx database "(.*)" should contain the measurement "(.*)" with following records:$""") {
    (timeout: String, database: String, measurement: String, table: java.util.List[String]) =>
      //wait some time
      Thread.sleep(Duration.apply(timeout).toMillis)
      val expectedResult = table.map(r => Utilities.replaceTimes(r)).toArray
      assertTrue("No database " + database + " in influx", databases.contains(database))
      compareExpectedResult(databases(database), measurement, expectedResult)
  }

  Then("""^after "(.*)" Influx database "(.*)" should contain the measurement "(.*)" with the records from the file "(.*)":$""") {
    (timeout: String, database: String, measurement: String, file: String) =>
      Thread.sleep(Duration.apply(timeout).toMillis)
      val expectedResult = Source.fromFile(file, "UTF-8").getLines.toList.map(r => Utilities.replaceTimes(r)).toArray
      assertTrue(databases.contains(database))
      compareExpectedResult(databases(database), measurement, expectedResult)

  }

  Then("""^the count of record in the measurement "(.*)" in the Influx is (\d+)$""") {
    (measurement: String, count: Int) =>
      val samplesDatabase: Database = databases("samples")
      val result = Await.result(samplesDatabase.query("select count(*) from " + measurement), Duration.Inf)
      assertEquals(BigDecimal(count), result.series.head.records.head("count_value"))
  }


}
