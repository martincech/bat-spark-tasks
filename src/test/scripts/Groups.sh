#!/bin/bash
kafka-console-producer --broker-list localhost:9092 --topic Groups << EOF
{"Uid":"Test-T-Test","Database":"db2","Groups":[{"Group":"Test-T-Test"}]}
{"Uid":"Test-Z-T-Test","Database":"db1","Groups":[{"Group":"Test-Z-T-Test"}]}
{"Uid":"2-T-88","Database":"db2","Groups":[{"Group":"2-T-88"}]}
{"Uid":"1-T-88","Database":"db1","Groups":[{"Group":"1-T-88"}]}
EOF

