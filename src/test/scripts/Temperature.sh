#!/bin/bash
kafka-console-producer --broker-list localhost:9092 --topic Temperature << EOF
{"TerminalSn":"Test","Data":{"TimeStamp":"2018-06-13T11:55:16","Value":6.9, "SensorUid":"Test" }}
{"TerminalSn":"Test","Data":{"TimeStamp":"2018-06-14T11:56:16Z","Value":6.9, "SensorUid":"Test-Z" }}
{"TerminalSn":88,"SensorPackSn":1,"Data":{"TimeStamp":"2018-06-13T11:55:16","Value":6.9}}
{"TerminalSn":88,"SensorPackSn":2,"Data":{"TimeStamp":"2018-06-14T11:56:16Z","Value":6.9}}
EOF
