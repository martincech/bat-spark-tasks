#!/bin/bash


Usage()
{ echo "$0 usage:" && grep "[[:space:]].)\ #" $0 | sed 's/#//' | sed -r 's/([a-z])\)/-\1/'; exit 0; }

export DATA_TOPICS="Temperature Humidity Co2 BirdWeight DailyWeight"
export SEX_TYPES="undefined male female"

export YESTERDAY=`date +"%Y-%m-%d" -d 'yesterday'`
export TODAY=`date +"%Y-%m-%d"`
export TOMORROW=`date +"%Y-%m-%d" -d 'tomorrow'`
export NEXT_MONTH=`date +"%Y-%m" -d 'next month'`
export NEXT_YEAR=`date +"%Y-%m" -d 'next year'`
export NEXT_YEAR_NEXT_MONTH=`date +"%Y-%m" -d 'next year next month'`
export DATABASE="db1"
export KAFKA_ADDRESS="localhost:9092"
export INFLUX_ADDRESS="localhost:8086"

while getopts hk:i:n Option ; do
	case ${Option} in
        k) #adress of kafka server
            KAFKA_ADDRESS=${OPTARG}
            ;;
        i) #adress of influx server
            INFLUX_ADDRESS=${OPTARG}
            ;;
        *) #Display help.
			Usage
			# Exit if only usage (-h) was specified.
			if [ "$#" -eq 1 ] ; then
				exit 10
			fi
			exit 0
			;;
	esac
done

echo "Generating data to"
echo "KAFKA:  " ${KAFKA_ADDRESS}
echo "INFLUX: " ${INFLUX_ADDRESS}


./generetaTestData.sh
./initInflux.sh
./publishToKafka.sh


