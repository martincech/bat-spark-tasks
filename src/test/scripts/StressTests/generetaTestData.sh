#!/bin/bash

DATA_TOPICS="${DATA_TOPICS:-"Temperature Humidity Co2 BirdWeight DailyWeight"}"
SEX_TYPES="${SEX_TYPES:-"undefined male female"}"
YESTERDAY="${YESTERDAY:-`date +"%Y-%m-%d" -d 'yesterday'`}"
TODAY="${TODAY:-`date +"%Y-%m-%d"`}"
TOMORROW="${TOMORROW:-`date +"%Y-%m-%d" -d 'tomorrow'`}"
NEXT_MONTH="${NEXT_MONTH:-`date +"%Y-%m" -d 'next month'`}"
NEXT_YEAR="${NEXT_YEAR:-`date +"%Y-%m" -d 'next year'`}"
NEXT_YEAR_NEXT_MONTH="${NEXT_YEAR_NEXT_MONTH:-`date +"%Y-%m" -d 'next year next month'`}"
DATABASE="${DATABASE:-"db1"}"


GenerateGroups()
{
#create group with single device and few groups
tr '\n' ' ' << EOF >> groups.$1
{"Uid":"$1-T-Test","Database":"${DATABASE}","Groups":[
{"Group":"device_$1-T-Test","From":"${NEXT_YEAR}-01T00:00:00Z","To":"${NEXT_YEAR}-28T00:00:00Z"},
{"Group":"event_future_${NEXT_YEAR}-01_${NEXT_YEAR}-14","From":"${NEXT_YEAR}-01T00:00:00Z","To":"${NEXT_YEAR}-14T00:00:00Z"},
{"Group":"house_future_${NEXT_YEAR}-01_${NEXT_YEAR}-14","From":"${NEXT_YEAR}-01T00:00:00Z","To":"${NEXT_YEAR}-14T00:00:00Z"},
{"Group":"device_$1-T-Test","From":"${YESTERDAY}T00:00:00Z","To":"${NEXT_MONTH}-28T00:00:00Z"},
{"Group":"event_active_${YESTERDAY}_${NEXT_MONTH}-28","From":"${YESTERDAY}T00:00:00Z","To":"${NEXT_MONTH}-28T00:00:00Z"},
{"Group":"house_active_${YESTERDAY}_${NEXT_MONTH}-28","From":"${YESTERDAY}T00:00:00Z","To":"${NEXT_MONTH}-28T00:00:00Z"},
{"Group":"event_future_${NEXT_YEAR}-15_${NEXT_YEAR}-28","From":"${NEXT_YEAR}-15T00:00:00Z","To":"${NEXT_YEAR}-28T00:00:00Z"},
{"Group":"house_future_${NEXT_YEAR}-15_${NEXT_YEAR}-28","From":"${NEXT_YEAR}-15T00:00:00Z","To":"${NEXT_YEAR}-28T00:00:00Z"}]}
EOF
}

GenerateRawData()
{
for SEX in $SEX_TYPES
do
BW=""
if [ $1 == "BirdWeight" ]
then

    BW=", \"Sex\": \"${SEX}\", \"Difference\": \"1\""
fi

#send some data, this should be in groups device_$1-T-Test, house_active_1 and event_active_1
#all the half hours day 1
for HOUR in {00..23}
do
if [ ${HOUR} == 00 ]
then
VALUE=0
else
VALUE=$(echo ${HOUR} | sed 's/^0*//')
fi
echo -ne "."
/bin/cat << EOF >> datafile.$1
{"TerminalSn":"Test","Data":{"TimeStamp":"${TODAY}T${HOUR}:00:00Z","Value":${VALUE}.0, "SensorUid":"$1"${BW}}}
{"TerminalSn":"Test","Data":{"TimeStamp":"${TODAY}T${HOUR}:30:00Z","Value":${VALUE}.5, "SensorUid":"$1"${BW}}}
EOF
done

#single half hour of day 2
for MINUTE in {30..39}
do
echo -ne "."
VALUE=$(echo ${MINUTE} | sed 's/^0*//')
/bin/cat << EOF >> datafile.$1
{"TerminalSn":"Test","Data":{"TimeStamp":"${TOMORROW}T00:${MINUTE}:00Z","Value":${VALUE}.0, "SensorUid":"$1"${BW}}}
EOF
done

#single day for the whole next month with duplication
for DAY in {01..28}
do
echo -ne "."
VALUE=$(echo ${DAY} | sed 's/^0*//')
/bin/cat << EOF >> datafile.$1
{"TerminalSn":"Test","Data":{"TimeStamp":"${NEXT_MONTH}-${DAY}T00:00:00Z","Value":${VALUE}.0, "SensorUid":"$1"${BW}}}
{"TerminalSn":"Test","Data":{"TimeStamp":"${NEXT_MONTH}-${DAY}T00:00:00Z","Value":${VALUE}.0, "SensorUid":"$1"${BW}}}
EOF
done

#no data at all for this time - out of time
for DAY in {01..28}
do
echo -ne "."
VALUE=$(echo ${DAY} | sed 's/^0*//')
/bin/cat << EOF >> datafile.$1
{"TerminalSn":"Test","Data":{"TimeStamp":"${NEXT_YEAR_NEXT_MONTH}-${DAY}T00:00:00Z","Value":${VALUE}.0, "SensorUid":"$1"${BW}}}
EOF
done

echo
#break the sex loop

if [ $1 != "BirdWeight" ]
then
    echo "Data for " $1 " generated"
    break
else
    echo "Data for " $1 " SEX: " ${SEX} " generated"
fi
done
}

GenerateStatisticsData()
{
for SEX in $SEX_TYPES
do

for HOUR in {00..23}
do
    if [ ${HOUR} == 00 ]
    then
        VALUE=0
    else
        VALUE=$(echo ${HOUR} | sed 's/^0*//')
    fi
echo -ne "."
#Value within one day, raising count, existing and non existing sensor
/bin/cat << EOF >> datafile.$1
{"Uid":"$1-T-Test","TimeStamp":"${TODAY}T${HOUR}:00:00Z","Count":${VALUE},"Day":1,"Average":${VALUE}.0,"Gain":1.0,"Sigma":1.0,"Cv":12.0,"Uniformity":25.0,"Sex":"${SEX}"}
{"Uid":"$2-T-Test","TimeStamp":"${TODAY}T${HOUR}:00:00Z","Count":${VALUE},"Day":1,"Average":${VALUE}.0,"Gain":1.0,"Sigma":1.0,"Cv":12.0,"Uniformity":25.0,"Sex":"${SEX}"}
EOF
done

#single day for the whole next month with duplication
for DAY in {01..28}
do
echo -ne "."
VALUE=$(echo ${DAY} | sed 's/^0*//')
/bin/cat << EOF >> datafile.$1
{"Uid":"$1-T-Test","TimeStamp":"${NEXT_MONTH}-${DAY}T00:00:00Z","Count":${VALUE},"Day":1,"Average":${VALUE}.0,"Gain":1.0,"Sigma":1.0,"Cv":12.0,"Uniformity":25.0,"Sex":"${SEX}"}
{"Uid":"$1-T-Test","TimeStamp":"${NEXT_MONTH}-${DAY}T00:00:00Z","Count":${VALUE},"Day":1,"Average":${VALUE}.0,"Gain":1.0,"Sigma":1.0,"Cv":12.0,"Uniformity":25.0,"Sex":"${SEX}"}
{"Uid":"$1-T-Test","TimeStamp":"${NEXT_MONTH}-${DAY}T23:59:59Z","Count":${VALUE},"Day":1,"Average":${VALUE}.5,"Gain":1.0,"Sigma":1.0,"Cv":12.0,"Uniformity":25.0,"Sex":"${SEX}"}
{"Uid":"$1-T-Test","TimeStamp":"${NEXT_MONTH}-${DAY}T23:59:59Z","Count":${VALUE},"Day":1,"Average":${VALUE}.5,"Gain":1.0,"Sigma":1.0,"Cv":12.0,"Uniformity":25.0,"Sex":"${SEX}"}
EOF
done

#no data at all for this time - out of time
for DAY in {01..28}
do
echo -ne "."
VALUE=$(echo ${DAY} | sed 's/^0*//')
/bin/cat << EOF >> datafile.$1
{"Uid":"$1-T-Test","TimeStamp":"${NEXT_YEAR_NEXT_MONTH}-${DAY}T00:00:00Z","Count":${VALUE},"Day":1,"Average":${VALUE}.0,"Gain":1.0,"Sigma":1.0,"Cv":12.0,"Uniformity":25.0,"Sex":"${SEX}"}
EOF
done

done
}


# create data
for TOPIC in $DATA_TOPICS
do
echo "Removing old files for " ${TOPIC}
if [ -e groups.${TOPIC} ]
then
    rm groups.${TOPIC}
fi

if [ -e datafile.${TOPIC} ]
then
    rm datafile.${TOPIC}
fi

echo "Generating data for " ${TOPIC}
GenerateGroups ${TOPIC}
if [ ${TOPIC} == "DailyWeight" ]
then
    GenerateStatisticsData ${TOPIC} "BirdWeight"
else
    GenerateRawData ${TOPIC}
fi
done

