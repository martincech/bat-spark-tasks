#!/bin/bash

DATABASE="${DATABASE:-"db1 db2"}"
INFLUX_ADDRESS="${INFLUX_ADDRESS:-"localhost:8086"}"

#set influx to the expected state
echo "Droping databases in influx"
curl -i -XPOST http://${INFLUX_ADDRESS}/query --data-urlencode "q=DROP DATABASE samples" 2&>1 > /dev/null
curl -i -XPOST http://${INFLUX_ADDRESS}/query --data-urlencode "q=DROP DATABASE ${DATABASE}" 2&>1 > /dev/null
echo "Recreating influx dabases"
curl -i -XPOST http://${INFLUX_ADDRESS}/query --data-urlencode "q=CREATE DATABASE samples" 2&>1 > /dev/null
curl -i -XPOST http://${INFLUX_ADDRESS}/query --data-urlencode "q=CREATE RETENTION POLICY \"raw\" ON \"samples\" DURATION 200w REPLICATION 1" 2&>1 > /dev/null
for DB in ${DATABASE}
do
curl -i -XPOST http://${INFLUX_ADDRESS}/query --data-urlencode "q=CREATE DATABASE ${DB}" 2&>1 > /dev/null
curl -i -XPOST http://${INFLUX_ADDRESS}/query --data-urlencode "q=CREATE RETENTION POLICY \"30_m\" ON \"${DB}\" DURATION 200w REPLICATION 1" 2&>1 > /dev/null
curl -i -XPOST http://${INFLUX_ADDRESS}/query --data-urlencode "q=CREATE RETENTION POLICY \"1440_m\" ON \"${DB}\" DURATION 200w REPLICATION 1" 2&>1 > /dev/null
done