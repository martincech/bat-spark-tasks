#!/bin/bash
DATA_TOPICS="${DATA_TOPICS:-"Temperature Humidity Co2 BirdWeight DailyWeight"}"
KAFKA_ADDRESS="${KAFKA_ADDRESS:-"localhost:9092"}"

# send data to kafka
for TOPIC in ${DATA_TOPICS}
do

if [ -e groups.${TOPIC} ]
then
    echo "Sending groups for " ${TOPIC}
    kafka-console-producer --broker-list ${KAFKA_ADDRESS} --topic Groups < groups.${TOPIC}
    echo "Data for " ${TOPIC} " send"
else
    echo "No groups file for " ${TOPIC}
fi
if [ -e datafile.${TOPIC} ]
then
    echo "Sending data for " ${TOPIC}
    kafka-console-producer --broker-list ${KAFKA_ADDRESS} --topic ${TOPIC} < datafile.${TOPIC}
    echo "Data for " ${TOPIC} " send"
else
    echo "No data file for " ${TOPIC}
fi
done


