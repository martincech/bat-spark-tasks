#!/bin/bash
kafka-console-producer --broker-list localhost:9092 --topic BirdWeight << EOF
{"TerminalSn":"Test","Data":{"TimeStamp":"2018-03-25T11:55:16","Value":6.9, "SensorUid":"Test", "Sex": 1, "Difference": 1 }}
{"TerminalSn":"Test","Data":{"TimeStamp":"2018-03-25T11:56:16Z","Value":6.9, "SensorUid":"Test-Z", "Sex": 1, "Difference": 1 }}
{"TerminalSn":88,"SensorPackSn":1,"Data":{"TimeStamp":"2018-03-25T11:55:16","Value":6.9, "Sex": 1, "Difference": 1}}
{"TerminalSn":88,"SensorPackSn":2,"Data":{"TimeStamp":"2018-03-25T11:56:16Z","Value":6.9, "Sex": 1, "Difference": 1}}

{"TerminalSn":"Test","Data":{"TimeStamp":"2018-03-25T12:55:16","Value":6.9, "SensorUid":"Test", "Sex": "undefined", "Difference": 1 }}
{"TerminalSn":"Test","Data":{"TimeStamp":"2018-03-25T13:55:16","Value":6.9, "SensorUid":"Test", "Sex": "male", "Difference": 1 }}
{"TerminalSn":"Test","Data":{"TimeStamp":"2018-03-25T14:55:16","Value":6.9, "SensorUid":"Test", "Sex": "female", "Difference": 1 }}

EOF
