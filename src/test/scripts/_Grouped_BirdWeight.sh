#!/bin/bash
kafka-console-producer --broker-list localhost:9092 --topic _Grouped_BirdWeight << EOF
{"database":"db1","groupId":"1-T-88","value":6.9,"timestamp":"2018-03-25T09:55:16Z","sex":"1","difference":"1"}
{"database":"db2","groupId":"2-T-88","value":6.9,"timestamp":"2018-03-25T11:56:16Z","sex":"1","difference":"1"}
{"database":"db2","groupId":"Test-T-Test","value":6.9,"timestamp":"2018-03-25T09:55:16Z","sex":"1","difference":"1"}
{"database":"db1","groupId":"Test-Z-T-Test","value":6.9,"timestamp":"2018-03-25T11:56:16Z","sex":"1","difference":"1"}
{"database":"db2","groupId":"Test-T-Test","value":6.9,"timestamp":"2018-03-25T10:55:16Z","sex":"undefined","difference":"1"}
{"database":"db2","groupId":"Test-T-Test","value":6.9,"timestamp":"2018-03-25T11:55:16Z","sex":"male","difference":"1"}
{"database":"db2","groupId":"Test-T-Test","value":6.9,"timestamp":"2018-03-25T12:55:16Z","sex":"female","difference":"1"}
EOF
