#!/bin/bash
kafka-console-producer --broker-list localhost:9092 --property "parse.key=true" --property "key.separator=:" --topic _Grouped_DailyBirdWeight << EOF
1-T-88/1:{"id":"Test-T-Test","database":"db2","intervalStart":"2018-03-25T00:00:00Z","intervalEnd":"2018-03-26T00:00:00Z","sex":"undefined","topic":"_Grouped_BirdWeight","count":1.0,"average":6.9,"min":6.9,"max":6.9,"sigma":0.0,"cv":0.0,"gain":0.0,"uniformity":100.0}
1-T-88/1:{"id":"Test-T-Test","database":"db2","intervalStart":"2018-03-25T00:00:00Z","intervalEnd":"2018-03-26T00:00:00Z","sex":"undefined","topic":"_Grouped_BirdWeight","count":3.0,"average":6.9,"min":6.9,"max":6.9,"sigma":0.0,"cv":0.0,"gain":0.0,"uniformity":100.0}
1-T-88/2:{"id":"Test-T-Test","database":"db2","intervalStart":"2018-03-25T00:00:00Z","intervalEnd":"2018-03-26T00:00:00Z","sex":"undefined","topic":"_Grouped_BirdWeight","count":5.0,"average":6.9,"min":6.9,"max":6.9,"sigma":0.0,"cv":0.0,"gain":0.0,"uniformity":100.0}
1-T-88/2:{"id":"Test-T-Test","database":"db2","intervalStart":"2018-03-25T00:00:00Z","intervalEnd":"2018-03-26T00:00:00Z","sex":"undefined","topic":"_Grouped_BirdWeight","count":7.0,"average":6.9,"min":6.9,"max":6.9,"sigma":0.0,"cv":0.0,"gain":0.0,"uniformity":100.0}
EOF
