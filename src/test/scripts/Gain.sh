#!/bin/bash

for HOUR in {00..23}
do
    if [ ${HOUR} == 00 ]
    then
        VALUE=0
    else
        VALUE=$(echo ${HOUR} | sed 's/^0*//')
    fi
echo -ne "."
#Value within one day, raising count, existing and non existing sensor
/bin/cat << EOF >> testData
{"database":"db1","groupId":"GAIN-T-GAIN","value":${VALUE}.0,"timestamp":"2018-03-03T${HOUR}:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":${VALUE}.5,"timestamp":"2018-03-03T${HOUR}:30:00Z"}
EOF
/bin/cat << EOF >> testData
{"database":"db1","groupId":"GAIN-T-GAIN","value":7,"timestamp":"2018-03-05T12:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":8,"timestamp":"2018-03-05T13:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":10,"timestamp":"2018-03-06T12:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":11,"timestamp":"2018-03-06T13:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":14,"timestamp":"2018-03-07T12:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":15,"timestamp":"2018-03-07T13:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":19,"timestamp":"2018-03-08T12:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":20,"timestamp":"2018-03-08T13:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":14,"timestamp":"2018-03-09T12:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":15,"timestamp":"2018-03-09T13:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":10,"timestamp":"2018-03-10T12:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":11,"timestamp":"2018-03-10T13:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":7,"timestamp":"2018-03-11T12:00:00Z"}
{"database":"db1","groupId":"GAIN-T-GAIN","value":8,"timestamp":"2018-03-11T13:00:00Z"}
EOF
done

kafka-console-producer --broker-list localhost:9092 --topic _Grouped_Co2 < testData

rm testData

