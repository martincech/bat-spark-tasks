#!/bin/bash
kafka-console-producer --broker-list localhost:9092 --topic DailyWeight << EOF
{"Uid":"Test-T-Test","TimeStamp":"2018-03-09T00:00:00Z","Count":0,"Day":4,"Average":0.0,"Gain":0.0,"Sigma":0.0,"Cv":0.0,"Uniformity":100.0,"Sex":"male"}
{"Uid":"Test-T-Test","TimeStamp":"2018-03-10T00:00:00Z","Count":3,"Day":4,"Average":0.113333337,"Gain":0.113333337,"Sigma":0.0115470337,"Cv":10.1885586,"Uniformity":67.0,"Sex":"female"}
{"Uid":"Test-T-Test","TimeStamp":"2018-03-11T00:00:00Z","Count":3,"Day":4,"Average":0.113333337,"Gain":0.113333337,"Sigma":0.0115470337,"Cv":10.1885586,"Uniformity":67.0,"Sex":"undefined"}
EOF
